FORMAT: 1A
HOST:localhost

# ESI14-RentIt
Excerpt of RentIt's API

# Group Plants
Notes related resources of the **plants API**


## Find plants [/rest/plants]
### Get plants [GET]

+ Response 200 (application/json)

    [
    {
        "_links": [],
        "idRes": 10002,
        "name": "Excavator",
        "description": "3 Tonne Mini excavator",
        "price": 200,
        "links": [
            {
                "rel": "self",
                "href": "http://localhost:3000/rest/plants/10002"
            }
        ]
    },
    {
        "_links": [],
        "idRes": 10003,
        "name": "Excavator",
        "description": "5 Tonne Midi excavator",
        "price": 250,
        "links": [
            {
                "rel": "self",
                "href": "http://localhost:3000/rest/plants/10003"
            }
        ]
    },
    {
        "_links": [],
        "idRes": 10004,
        "name": "Excavator",
        "description": "8 Tonne Midi excavator",
        "price": 300,
        "links": [
            {
                "rel": "self",
                "href": "http://localhost:3000/rest/plants/10004"
            }
        ]
    },
    {
        "_links": [],
        "idRes": 10005,
        "name": "Excavator",
        "description": "15 Tonne Large excavator",
        "price": 400,
        "links": [
            {
                "rel": "self",
                "href": "http://localhost:3000/rest/plants/10005"
            }
        ]
    },
    {
        "_links": [],
        "idRes": 10006,
        "name": "Excavator",
        "description": "20 Tonne Large excavator",
        "price": 450,
        "links": [
            {
                "rel": "self",
                "href": "http://localhost:3000/rest/plants/10006"
            }
        ]
    }
]

## Find plants [/rest/plants/10002]
### Get Plant [GET]

+ Response 200 (application/json)

    {
        "_links": [],
        "idRes": 10002,
        "name": "Excavator",
        "description": "3 Tonne Mini excavator",
        "price": 200,
        "links": [
            {
                "rel": "self",
                "href": "http://localhost:3000/rest/plants/10002"
            }
        ]
    }

# Group Purchase Orders
Notes related resources of the **purchase orders API**


## Creating Purchase Order [/rest/pos]
### Create PO [POST]

+ Request (application/json)

    {
        "plant": {
            "idRes": 10001
        },
        "startDate": "2014-09-25",
        "endDate": "2014-09-27"
    }
        
+ Response 200 (application/json)

    {
        "_links": [
            {
                "rel": "rejectPO",
                "href": "http://localhost:3000/rest/pos/10001/accept",
                "method": "DELETE"
            },
            {
                "rel": "acceptPO",
                "href": "http://localhost:3000/rest/pos/10001/accept",
                "method": "POST"
            }
        ],
        "idRes": 10001,
        "plant": {
            "_links": [],
            "idRes": 10002,
            "name": "Excavator",
            "description": "3 Tonne Mini excavator",
            "price": 200,
            "links": [
                {
                    "rel": "self",
                    "href": "http://localhost:3000/rest/plants/10002"
                }
            ]
        },
        "customer": {
            "_links": [],
            "idRes": 10001,
            "name": "BuildIt",
            "vatNumber": "20100915ABCD",
            "links": []
        },
        "startDate": 1411603200000,
        "endDate": 1411776000000,
        "cost": 400,
        "links": [
            {
                "rel": "self",
                "href": "http://localhost:3000/rest/pos/10001"
            }
        ]
    }

+ Response 409 (application/json)

    {
        "_links": [
            {
                "rel": "rejectPO",
                "href": "http://localhost:3000/rest/pos/10001/accept",
                "method": "DELETE"
            },
            {
                "rel": "acceptPO",
                "href": "http://localhost:3000/rest/pos/10001/accept",
                "method": "POST"
            }
        ],
        "idRes": 10001,
        "plant": {
            "_links": [],
            "idRes": 10002,
            "name": "Excavator",
            "description": "3 Tonne Mini excavator",
            "price": 200,
            "links": [
                {
                    "rel": "self",
                    "href": "http://localhost:3000/rest/plants/10002"
                }
            ]
        },
        "customer": {
            "_links": [],
            "idRes": 10001,
            "name": "BuildIt",
            "vatNumber": "20100915ABCD",
            "links": []
        },
        "startDate": 1411603200000,
        "endDate": 1411776000000,
        "cost": 400,
        "links": [
            {
                "rel": "self",
                "href": "http://localhost:3000/rest/pos/10001"
            }
        ]
    }

## Purchase Order [/rest/pos/10001]

### Get PO [GET]

+ Response 200 (application/json)

    {
        "_links": [
            {
                "rel": "rejectPO",
                "href": "http://localhost:3000/rest/pos/10001/accept",
                "method": "DELETE"
            },
            {
                "rel": "acceptPO",
                "href": "http://localhost:3000/rest/pos/10001/accept",
                "method": "POST"
            }
        ],
        "idRes": 10001,
        "plant": {
            "_links": [],
            "idRes": 10002,
            "name": "Excavator",
            "description": "3 Tonne Mini excavator",
            "price": 200,
            "links": [
                {
                    "rel": "self",
                    "href": "http://localhost:3000/rest/plants/10002"
                }
            ]
        },
        "customer": {
            "_links": [],
            "idRes": 10001,
            "name": "BuildIt",
            "vatNumber": "20100915ABCD",
            "links": []
        },
        "startDate": 1411603200000,
        "endDate": 1411776000000,
        "cost": 400,
        "links": [
            {
                "rel": "self",
                "href": "http://localhost:3000/rest/pos/10001"
            }
        ]
    }
    
### Close PO [DELETE] 

+ Parameters 
    + po.id (required, number, `10001`) ... Numeric `po.id` of the Purchase Order.

+ Response 200 (application/json)

    {
        "_links": [
        ],
        "idRes": 10001,
        "plant": {
            "_links": [],
            "idRes": 10002,
            "name": "Excavator",
            "description": "3 Tonne Mini excavator",
            "price": 200,
            "links": [
                {
                    "rel": "self",
                    "href": "http://localhost:3000/rest/plants/10002"
                }
            ]
        },
        "customer": {
            "_links": [],
            "idRes": 10001,
            "name": "BuildIt",
            "vatNumber": "20100915ABCD",
            "links": []
        },
        "startDate": 1411603200000,
        "endDate": 1411776000000,
        "cost": 400,
        "links": [
            {
                "rel": "self",
                "href": "http://localhost:3000/rest/pos/10001"
            }
        ]
    }