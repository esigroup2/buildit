

var app = angular.module('project', [ "ngRoute", "ngCookies" ]);

app.factory('Data', function () {
    return { phrHref: "" };
});

app.directive('restrict', function($cookieStore){
    return{
        restrict: 'A',
        prioriry: 100000,
        scope: false,
        link: function(){
            // alert('ergo sum!');
        },
        compile:  function(element, attr, linker) {
            var accessDenied = true;

            var attributes = attr.access.split(" ");
            var userRoles = $cookieStore.get("userRoles");

            console.log("-------------");
            console.log(attributes);
            console.log(userRoles);
            console.log("-------------");

            var matchingRoles = userRoles.filter(function (role) {
                return attributes.indexOf(role) != -1;
            });

            if (userRoles !== undefined && matchingRoles.length > 0)
                accessDenied = false;


            if(accessDenied){
                element.children().remove();
                element.remove();
            }

        }
    }
});

