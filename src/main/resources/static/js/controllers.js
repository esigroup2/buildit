var app = angular.module('project');

app.controller('PlantsController', function ($scope, $http) {
    $scope.plants = [];
    $http.get('/rest/plants').success(function (data, status, headers, config) {
        console.log(JSON.stringify(data));
        $scope.plants = data;
    });
});

app.controller('InvoiceController', function ($scope, $http, $route, $location) {
    $scope.invoices = [];
    $http.get('/rest/invoices').success(function (data, status, headers, config) {
        console.log(JSON.stringify(data));
        $scope.invoices = data;
    });

    $scope.follow = function (link) {
        console.log(link);
        console.log(link.method);

        if (link.rel == "accept") {
            $http.post(link.href).success(function (data, status, headers, config) {
                console.log(data);
                $route.reload();
            });
        } else if (link.rel == 'reject') {
            $http.delete(link.href).success(function (data, status, headers, config) {
                console.log(data);
                $route.reload();
            })
        }
    };
});

app.controller('PHRsController', function ($scope, $http) {
    $scope.name = '';
    $scope.startDate = new Date();
    $scope.endDate = new Date();
    $scope.catalogShown = false;
    $scope.plants = [];

    $scope.execQuery = function () {
        $http.get('/rest/plants'
            ,{params: {name: $scope.name, startDate: formatDate($scope.startDate), endDate:formatDate( $scope.endDate)}}
        ).success(function (data, status, headers, config) {
                console.log(JSON.stringify(data));
                $scope.plants = data;
                $scope.catalogShown = true;
            });
    };
});

app.controller('PHRController', function ($scope, $http, $route, $location, Data) {
    $scope.phrs = [];
    $scope.data = Data;
    $scope.error = "";
    $http.get('/rest/phrs').success(function (data, status, headers, config) {
        console.log(data);
        $scope.phrs = data;
    });

    $scope.calculateCost = function (items) {
        var cost = 0;
        for (var i = 0; i < items.length; i++) {
            cost += items[i].price;
        }
        return cost;
    }

    $scope.hide = function (index) {
        $("#items-" + index).hide();
        $("#show-btn-" + index).show();
    }

    $scope.show = function (index) {
        $("#items-" + index).show();
        $("#show-btn-" + index).hide();
    }

    $scope.followItem = function (phr, item, link) {
        console.log(link);
        console.log(link.method);

        if (link.rel == "update") {
            $scope.data.href = link.href;
            $location.path('/phrs/' + phr.idRes + '/items/' + item.idRes + '/update');
        } else
            $scope.follow(link);
    };

    $scope.follow = function (link) {
        console.log(link);
        console.log(link.method);

        if (link.rel == "update") {
            $scope.data.href = link.href;
            $location.path('/phrs/update');
        } else if (link.rel == "extend") {
            $scope.data.href = link.href;
            $location.path('/phrs/extend');
        } else if (link.method == 'POST') {
            $http.post(link.href).success(function (data, status, headers, config) {
                console.log(data);
                $route.reload();
            });
        } else if (link.method == 'DELETE') {
            $http.delete(link.href).success(function (data, status, headers, config) {
                console.log(data);
                $route.reload();
            }).
                error(function (data, status, headers, config) {
                    $scope.error = "You can't cancel plant hire request now.";
                });
        }
    };
});


app.controller('CreatePHRController', function ($scope, $http, $location) {
    $scope.plants = [];
    $scope.name = '';
    $scope.startDate = new Date();
    $scope.endDate = new Date();
    $scope.orderedPlants = [];

    $scope.catalogShown = false;

    $scope.execQuery = function () {
        $scope.plants = [];
        $http.get('/rest/plants'
            ,{params: {name: $scope.name, startDate: formatDate($scope.startDate), endDate: formatDate($scope.endDate)}}
        ).success(function (data, status, headers, config) {
                $scope.plants = data;
                $scope.catalogShown = true;
            });
    };

    $scope.addPlant = function (plant) {
        var index = $scope.plants.indexOf(plant)
        $scope.plants.splice(index, 1);

        $scope.orderedPlants.push(plant);
    };

    $scope.removePlant = function (plant) {
        var index = $scope.orderedPlants.indexOf(plant)
        $scope.orderedPlants.splice(index, 1);

        $scope.plants.push(plant);
    };

    $scope.createPHR = function () {

        var items = [];
        for (var i = 0; i < $scope.orderedPlants.length; i++) {
            var item = {plant: $scope.orderedPlants[i], startDate: $scope.startDate, endDate: $scope.endDate};
            items.push(item);
        }

        phr = {items: items};

        $http.post('/rest/phrs', phr).success(function (data, status, headers, config) {
            console.log(status);
            $location.path('/phrs');
        });
    };
});

app.controller('UpdatePHRController', function ($scope, $http, $location, Data) {
    $scope.plants = [];
    $scope.name = '';
    $scope.startDate = new Date();
    $scope.endDate = new Date();
    $scope.data = Data;
    $scope.error = "";
    $scope.catalogShown = false;
    $scope.orderedPlants = [];
    $scope.items = [];

    init();

    function init() {
        console.log($scope.data.href);
        $http.get($scope.data.href).success(function (data, status, headers, config) {
            console.log(data);
            console.log(status);
            $scope.items = data.items;
        });
    }

    $scope.execQuery = function () {
        $http.get('/rest/plants'
            ,{params: {name: $scope.name, startDate: formatDate($scope.startDate), endDate: formatDate($scope.endDate)}}
        ).success(function (data, status, headers, config) {
                console.log(data);
                $scope.plants = data;
                $scope.catalogShown = true;
            });
    };

    $scope.addPlant = function (plant) {
        var index = $scope.plants.indexOf(plant)
        $scope.plants.splice(index, 1);

        $scope.orderedPlants.push(plant);
    };

    $scope.removePlant = function (plant) {
        var index = $scope.orderedPlants.indexOf(plant)
        $scope.orderedPlants.splice(index, 1);

        $scope.plants.push(plant);
    };

    $scope.removeItem = function (item) {
        var index = $scope.orderedPlants.indexOf(item)
        $scope.items.splice(index, 1);
    };

    $scope.updatePHR = function () {

        for (var i = 0; i < $scope.orderedPlants.length; i++) {
            var item = {plant: $scope.orderedPlants[i], startDate: $scope.startDate, endDate: $scope.endDate};
            $scope.items.push(item);
        }

        phr = {items: $scope.items};

        $scope.orderedPlants = [];
        console.log(phr);

        $http.post($scope.data.href, phr).success(function (data, status, headers, config) {
            console.log(status);
            $location.path('/phrs');
        });
    }

});

app.controller('ExtendPHRController', function ($scope, $http, $location, Data) {
    $scope.plants = [];
    $scope.name = '';
    $scope.startDate = new Date();
    $scope.endDate = new Date();
    $scope.data = Data;
    $scope.error = "";
    $scope.catalogShown = false;

    init();

    function init() {
        console.log($scope.data.href);
        $http.get($scope.data.href).success(function (data, status, headers, config) {
            console.log(data);
            console.log(status);
            var parts = data.startDate.split("-");
            $scope.startDate = new Date(parseInt(parts[0], 10),
                parseInt(parts[1], 10) - 1,
                parseInt(parts[2], 10));

            var parts = data.endDate.split("-");
            $scope.endDate = new Date(parseInt(parts[0], 10),
                parseInt(parts[1], 10) - 1,
                parseInt(parts[2], 10));
        });
    }

    $scope.extend = function () {
        phr = {startDate: $scope.startDate, endDate: $scope.endDate};

        $http.post($scope.data.href, phr).success(function (data, status, headers, config) {
            console.log(status);
            $location.path('/phrs');
        }).
            error(function (data, status, headers, config) {
                $scope.error = "Extension dates are incorrect.";
            });
    };
});

app.controller("LoginController", function ($scope, $http, $location, $rootScope, $cookieStore) {
    $scope.username = "";
    $scope.password = "";
    delete $rootScope.user;
    delete $rootScope.code;
    $cookieStore.remove("user");
    $cookieStore.remove("code");
    $cookieStore.remove("userRoles");

    $scope.login = function () {
        var code = window.btoa($scope.username + ":" + $scope.password);
        console.log(code);
        $rootScope.user = $scope.username;
        $rootScope.code = code;
        $http.post("/rest/authentication")
            .success(function (data, status, headers, config) {
                console.log(data.roles);
                $cookieStore.put("userRoles", data.roles);
                $cookieStore.put("user", $scope.username);
                $cookieStore.put("code", code);
                $location.path("/dashboard");
            });
    };
});

app.controller("RegisterController", function ($scope, $http, $location, $rootScope, $cookieStore) {
    $scope.username = "";
    $scope.password = "";
    delete $rootScope.user;
    delete $rootScope.code;
    $cookieStore.remove("user");
    $cookieStore.remove("code");
    $cookieStore.remove("userRoles");

    $scope.register = function () {
        var user = {username: $scope.username, password: $scope.password};
        $http.post("/registration", user)
            .success(function (data, status, headers, config) {

                var code = window.btoa($scope.username + ":" + $scope.password);
                $rootScope.user = $scope.username;
                $rootScope.code = code;

                $http.post("/rest/authentication")
                    .success(function (data, status, headers, config) {
                        console.log(data.roles);
                        $cookieStore.put("userRoles", data.roles);
                        $cookieStore.put("user", $scope.username);
                        $cookieStore.put("code", code);
                        $location.path("/phrs");
                    });
            })
            .error(function (data, status, headers, config) {
                alert("error");
            });
    };
});

app.run(function ($rootScope, $location, $cookieStore) {
    var user = $cookieStore.get("user");
    var code = $cookieStore.get("code");
    if (user !== undefined && code !== undefined) {
        $rootScope.user = user;
        $rootScope.code = code;
    }

    $rootScope.logout = function () {
        delete $rootScope.user;
        delete $rootScope.code;

        $cookieStore.remove("user");
        $cookieStore.remove("code");
        $cookieStore.remove("userRoles");
        $location.path("/login");
    };
});

app.service("RoleBasedAccessService", function ($rootScope, $location, $q,
                                                $cookieStore) {
    return {
        permissionCheck: function (roleCollection) {
            var deferred = $q.defer();
            var userRoles = $cookieStore.get("userRoles");

            console.log("-------------");
            console.log(roleCollection);
            console.log(userRoles);
            console.log("-------------");

            var matchingRoles = userRoles.filter(function (role) {
                return roleCollection.indexOf(role) != -1;
            });
            if (userRoles !== undefined && matchingRoles.length > 0)
                deferred.resolve();
            else {
                $location.path("/login");
                $rootScope.$on("$locationChangeSuccess",
                    function (next, current) {
                        deferred.resolve();
                    });
            }
            return deferred.promise;
        }
    };
});

app.controller('UserController', function ($scope, $http, $routeParams) {
    $scope.role = "ROLE_"
    $http.get('/rest/users/' + $routeParams.username).success(function (data, status, headers, config) {
        console.log(data);
        $scope.user = data;
    });

    var loadAuthorities = function () {
        $http.get('/rest/users/' + $routeParams.username + '/authorities')
            .success(function (data, status, headers, config) {
                console.log(data);
                $scope.authorities = data;
            });
    }

    $scope.del = function (id) {
        $http.delete('/rest/users/' + $scope.user.username + '/authorities/' + id)
            .success(function (data, status, headers, config) {
                console.log(data);
                loadAuthorities();
            });
    }

    $scope.add = function () {
        var authority = {username: $scope.user.username, authority: $scope.role};
        $http.post('/rest/users/' + $scope.user.username + '/authorities', authority).success(function (data, status, headers, config) {
            console.log(data);
            loadAuthorities();
            $scope.role = "ROLE_"
        });
    }

    loadAuthorities();
});

app.controller('UsersController', function ($scope, $http) {
    $scope.users = [];
    $http.get('/rest/users').success(function (data, status, headers, config) {
        console.log(data);
        $scope.users = data;
    });
});

app.controller('UpdateItemController', function ($scope, $http, $location, Data, $routeParams) {
    $scope.plants = [];
    $scope.name = '';
    $scope.startDate = new Date();
    $scope.endDate = new Date();
    $scope.data = Data;
    $scope.error = "";
    $scope.catalogShown = false;

    $scope.execQuery = function () {
        $http.get('/rest/plants'
            ,{params: {name: $scope.name, startDate: formatDate($scope.startDate), endDate: formatDate($scope.endDate)}}
        ).success(function (data, status, headers, config) {
                console.log(data);
                $scope.plants = data;
                $scope.catalogShown = true;
            });
    };

    $scope.addPlant = function (plant) {
        var item = {plant: plant, startDate: $scope.startDate, endDate: $scope.endDate};
        $http.post("/rest/phrs/" + $routeParams.id + "/items/" + $routeParams.itemId, item).success(function (data, status, headers, config) {
            console.log(status);
            $location.path('/phrs');
        });
    };

});

app.controller('DashboardController', function ($scope, $http) {
});


function formatDate(d){
    var curr_date = d.getDate();
    var curr_month = d.getMonth() + 1;
    var curr_year = d.getFullYear();
    return (curr_year + "-" + curr_month + "-" + curr_date);
}