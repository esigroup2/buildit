var app = angular.module('project');

app.config(function ($routeProvider, $locationProvider, $httpProvider) {
    $routeProvider
        .when('/dashboard', {
            controller: 'DashboardController',
            templateUrl: 'views/dashboard.html',
            resolve: {
                permission: function (RoleBasedAccessService, $route) {
                    return RoleBasedAccessService.permissionCheck(["ROLE_SE", "ROLE_WE", "ROLE_ADMIN"]);
                }
            }
        })
        .when('/plants', {
            controller: 'PlantsController',
            templateUrl: 'views/plants/list.html',
            resolve: {
                permission: function (RoleBasedAccessService, $route) {
                    return RoleBasedAccessService.permissionCheck(["ROLE_SE", "ROLE_WE", "ROLE_ADMIN"]);
                }
            }
        })
        .when('/phrs', {
            controller: 'PHRController',
            templateUrl: 'views/phrs/list.html',
            resolve: {
                permission: function (RoleBasedAccessService, $route) {
                    return RoleBasedAccessService.permissionCheck(["ROLE_SE", "ROLE_WE", "ROLE_ADMIN"]);
                }
            }
        })
        .when('/phrs/create', {
            controller: 'CreatePHRController',
            templateUrl: 'views/phrs/create.html',
            resolve: {
                permission: function (RoleBasedAccessService, $route) {
                    return RoleBasedAccessService.permissionCheck(["ROLE_SE", "ROLE_ADMIN"]);
                }
            }
        })
        .when('/phrs/update', {
            controller: 'UpdatePHRController',
            templateUrl: 'views/phrs/update.html',
            resolve: {
                permission: function (RoleBasedAccessService, $route) {
                    return RoleBasedAccessService.permissionCheck(["ROLE_SE", "ROLE_ADMIN"]);
                }
            }
        })
        .when('/phrs/extend', {
            controller: 'ExtendPHRController',
            templateUrl: 'views/phrs/extend.html',
            resolve: {
                permission: function (RoleBasedAccessService, $route) {
                    return RoleBasedAccessService.permissionCheck(["ROLE_SE", "ROLE_ADMIN"]);
                }
            }
        })
        .when('/phrs/:id/items/:itemId/update', {
            controller: 'UpdateItemController',
            templateUrl: 'views/phrs/items/update.html',
            resolve: {
                permission: function (RoleBasedAccessService, $route) {
                    return RoleBasedAccessService.permissionCheck(["ROLE_SE", "ROLE_ADMIN"]);
                }
            }
        })
        .when('/invoices', {
            controler: 'InvoiceController',
            templateUrl: 'views/invoices/list.html',
            resolve: {
                permission: function (RoleBasedAccessService, $route) {
                    return RoleBasedAccessService.permissionCheck(["ROLE_SE", "ROLE_ADMIN"]);
                }
            }
        })
        .when('/register', {
            controller: 'RegisterController',
            templateUrl: 'views/registration.html'
        })
        .when('/login', {
            controller: 'LoginController',
            templateUrl: 'views/login.html'
        })
        .when('/users/:username', {
            controller: 'UserController',
            templateUrl: 'views/users/user.html',
            resolve: {
                permission: function (RoleBasedAccessService, $route) {
                    return RoleBasedAccessService.permissionCheck(["ROLE_ADMIN"]);
                }
            }
        })
        .when('/users', {
            controller: 'UsersController',
            templateUrl: 'views/users/list.html',
            resolve: {
                permission: function (RoleBasedAccessService, $route) {
                    return RoleBasedAccessService.permissionCheck(["ROLE_ADMIN"]);
                }
            }
        })
        .when('/register', {
            controller: 'RegisterController',
            templateUrl: 'views/registration.html'
        })
        .otherwise({
            redirectTo: '/login'
        });

    $httpProvider.interceptors
        .push(function ($q, $rootScope, $location, $window) {
            return {
                'request': function (config) {
                    var isRestCall = config.url.indexOf('/rest') == 0;
                    if (isRestCall) {
                        config.headers['Authorization'] = 'Basic '
                        + $rootScope.code;
                    }
                    return config || $q.when(config);
                },
                'responseError': function (rejection) {
                    var status = rejection.status;
                    var config = rejection.config;
                    var method = config.method;
                    var url = config.url;

                    if (status == 401) {
                        // $rootScope.logout();
                        $window.history.back();
                    } else {
                        $rootScope.error = method + " on " + url
                        + " failed with status " + status;
                    }

                    return $q.reject(rejection);
                }
            };
        });
});