insert into users (username, password, enabled) SELECT 'admin', 'admin', true where not exists (select username, password, enabled from users where username = 'admin' and password = 'admin')
insert into authorities (id, username, authority) SELECT 100, 'admin', 'ROLE_ADMIN' where not exists (select username, authority from authorities where username = 'admin' and authority = 'ROLE_ADMIN')

insert into users (username, password, enabled) SELECT 'se', 'se', true where not exists (select username, password, enabled from users where username = 'se' and password = 'se')
insert into authorities (id, username, authority) SELECT 101, 'se', 'ROLE_SE' where not exists (select username, authority from authorities where username = 'se' and authority = 'ROLE_SE')

insert into users (username, password, enabled) SELECT 'we', 'we', true where not exists (select username, password, enabled from users where username = 'we' and password = 'we')
insert into authorities (id, username, authority) SELECT 102, 'we', 'ROLE_WE' where not exists (select username, authority from authorities where username = 'we' and authority = 'ROLE_WE')

-- insert into supplier (id, name, host, port, email) SELECT 1,'our_rentit', 'ostap0207rentit.herokuapp.com','80', 'esigroup2.rentit@gmail.com' where not exists (select name, host, port from supplier where name = 'our_rentit' and host = 'ostap0207rentit.herokuapp.com' and port = '80')
insert into supplier (id, name, host, port, email) SELECT 1,'our_rentit', 'localhost','3000','esigroup2.rentit@gmail.com' where not exists (select name, host, port from supplier where name = 'our_rentit' and host = 'localhost' and port = '3000')
insert into supplier (id, name, host, port, email) SELECT 2,'team3', 'rentit-team3.herokuapp.com','80','rentit.ut@gmail.com' where not exists (select name, host, port from supplier where name = 'team3' and host = 'rentit-team3.herokuapp.com' and port = '80')
insert into supplier (id, name, host, port, email) SELECT 3,'team4', 'rentit-1102.herokuapp.com','80','octa.rentit.esi@gmail.com' where not exists (select name, host, port from supplier where name = 'team4' and host = 'rentit-1102.herokuapp.com' and port = '80')