package demo.controllers;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import demo.integration.RequestGateway;
import demo.integration.dto.PlantHireRequestResource;
import demo.integration.dto.PlantResource;
import demo.services.PlantHireRequestManager;
import demo.services.rentit.RentalService;


@Controller
@RequestMapping("/rest/plants")
public class PlantController {
	
	private static final String DATE_FORMAT = "yyyy-MM-dd";
	
	@Autowired
	RentalService rentitRest;
	@Autowired
	RequestGateway rentit;



    @RequestMapping
    public ResponseEntity<List<PlantResource>> getPlants(){
        return new ResponseEntity<List<PlantResource>>(rentitRest.getPlants(), HttpStatus.OK);
    }

	@RequestMapping(value="/int")
	public @ResponseBody List<PlantResource> getAllPlants() throws IOException {				
		return rentit.getAllPlants();
	}
	@RequestMapping(params = {"name","startDate","endDate"})
	public ResponseEntity<List<PlantResource>> getPlantByNameBetweenDates(@RequestParam String name,
			@DateTimeFormat(pattern = DATE_FORMAT) @RequestParam Date startDate,
			@DateTimeFormat(pattern = DATE_FORMAT) @RequestParam Date endDate){
		return new ResponseEntity<List<PlantResource>>(rentitRest.findAvailablePlants(name, startDate, endDate), HttpStatus.OK);
	}
	
	
	
}
