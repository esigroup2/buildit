package demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import demo.integration.dto.assemblers.InvoiceAssembler;
import demo.integration.dto.InvoiceResource;
import demo.models.Invoice;
import demo.services.InvoiceManager;

@RestController
@RequestMapping("/rest/invoices")
public class InvoiceController {

	@Autowired
	InvoiceManager invoiceManager;
	InvoiceAssembler invoiceAssembler = new InvoiceAssembler();
	
	@RequestMapping
	public ResponseEntity<List<InvoiceResource>> getAll(){
		return new ResponseEntity<List<InvoiceResource>>(invoiceAssembler.toResources(invoiceManager.getUnpaidInvoices()), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/{id}/accept")
	public ResponseEntity<InvoiceResource> acceptInvoice(@PathVariable Long id) throws Exception{
		Invoice inv = invoiceManager.acceptInvoice(id);
		return new ResponseEntity<InvoiceResource>(invoiceAssembler.toResource(inv), HttpStatus.ACCEPTED);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value="/{id}/accept")
	public ResponseEntity<InvoiceResource> rejectInvoice(@PathVariable Long id) throws Exception{
		Invoice inv = invoiceManager.rejectInvoice(id);
		return new ResponseEntity<InvoiceResource>(invoiceAssembler.toResource(inv), HttpStatus.OK);
	}
}
