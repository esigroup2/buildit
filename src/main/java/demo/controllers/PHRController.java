package demo.controllers;

import demo.exceptions.CannotCancelException;
import demo.exceptions.WrongArgumentException;
import demo.integration.dto.*;
import demo.integration.dto.assemblers.LineItemAssembler;
import demo.integration.dto.assemblers.PlantHireRequestAssembler;
import demo.integration.dto.assemblers.UpdatePHRAssembler;
import demo.models.LineItem;
import demo.models.PHRStatus;
import demo.models.PlantHireRequest;
import demo.models.UpdatePHR;
import demo.models.repositories.LineItemRepository;
import demo.models.repositories.PlantHireRequestRepository;
import demo.models.repositories.SupplierRepository;
import demo.models.repositories.UpdatePHRRepository;
import demo.services.PlantHireRequestManager;
import demo.util.Utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/rest/phrs")
public class PHRController {
	

    @Autowired
    @Qualifier("plantHireRequestRepository")
    PlantHireRequestRepository phrRepo;

    
    @Autowired
    UpdatePHRRepository updatePHRRepo;

	@Autowired
	LineItemRepository lineItemRepo;

	@Autowired
	SupplierRepository supplierRepo;

	@Autowired
	PlantHireRequestManager phrManager;
	PlantHireRequestAssembler phrAssembler = new PlantHireRequestAssembler();
	UpdatePHRAssembler updatePHRAssembler = new UpdatePHRAssembler();
	LineItemAssembler lineItemAssembler = new LineItemAssembler();

	
	@RequestMapping
	@Secured({"ROLE_SE", "ROLE_WE", "ROLE_ADMIN"})
	public ResponseEntity<List<PlantHireRequestResource>> getAll(){
		return new ResponseEntity<List<PlantHireRequestResource>>(phrAssembler.toResources(phrRepo.findAll()), HttpStatus.OK);
	}
	
	@Secured({"ROLE_SE", "ROLE_ADMIN"})
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<PlantHireRequestResource> createRequest(@RequestBody PlantHireRequestResource phrr){
		PlantHireRequest phr = new PlantHireRequest();

		List<LineItem> lineItems = new ArrayList<>();
		for (LineItemResource lineItemResource : phrr.getItems()) {
			LineItem item = new LineItem();
			item.setPlantRef(lineItemResource.getPlant().getId().getHref());

			float price = lineItemResource.getPlant().getPrice();
			item.setPrice(Utils.getPeriod(lineItemResource.getStartDate(), lineItemResource.getEndDate()) * price);
			item.setStartDate(lineItemResource.getStartDate());
			item.setEndDate(lineItemResource.getEndDate());
			item.setSupplier(supplierRepo.getOne(lineItemResource.getPlant().getSupplierId()));

			item = lineItemRepo.saveAndFlush(item);
			lineItems.add(item);
		}

	    phr.setItems(lineItems);

		return new ResponseEntity<PlantHireRequestResource>(phrAssembler.toResource(phrManager.createPlantHireRequest(phr)), HttpStatus.CREATED);
	}
	
	@Secured({"ROLE_SE", "ROLE_ADMIN"})
	@RequestMapping(method = RequestMethod.GET, value="/{id}/status")
	public ResponseEntity<PHRStatus> checkStatus(@PathVariable Long id){
		return new ResponseEntity<PHRStatus> (phrRepo.findOne(id).getStatus(), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value="/{id}/approve")
	public ResponseEntity<PlantHireRequestResource> approveRequest(@PathVariable Long id) throws Exception{
		PlantHireRequestResource phrr = phrAssembler.toResource(phrManager.approvePlantHireRequest(id));
		return new ResponseEntity<PlantHireRequestResource>(phrr, HttpStatus.ACCEPTED);
	}

	@RequestMapping(method = RequestMethod.DELETE, value="/{id}/approve")
	public ResponseEntity<PlantHireRequestResource> rejectRequest(@PathVariable Long id){
		PlantHireRequestResource phrr = phrAssembler.toResource(phrManager.rejectPlantHireRequest(id));
		return new ResponseEntity<PlantHireRequestResource>(phrr, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value="/{id}")
	public ResponseEntity<PlantHireRequestResource> updateRequest(@PathVariable Long id,@RequestBody PlantHireRequestResource updatedPhr) throws Exception{
		PlantHireRequest phr = phrManager.updatePlantHireRequest(id, updatedPhr);
		PlantHireRequestResource phrr = phrAssembler.toResource(phr);
		return new ResponseEntity<PlantHireRequestResource>(phrr, HttpStatus.OK);
	}

//	@Secured({"ROLE_SE", "ROLE_ADMIN"})
	@RequestMapping(method = RequestMethod.DELETE, value="/{id}/items/{itemId}")
	public ResponseEntity<String> cancelLineItemInRequest(@PathVariable Long id, @PathVariable Long itemId) throws Exception{
		phrManager.cancelItemInPHR(id, itemId);
	    return new ResponseEntity<String>("Deleted", HttpStatus.OK);
	}
	 
	@RequestMapping(method = RequestMethod.GET, value="/{id}")
	public ResponseEntity<PlantHireRequestResource> getRequest(@PathVariable Long id){
		PlantHireRequest phr = phrManager.getPlantHireRequest(id);
		PlantHireRequestResource phrr = phrAssembler.toResource(phr);
		return new ResponseEntity<PlantHireRequestResource>(phrr, HttpStatus.OK);
	}
	
//	@Secured({"ROLE_WE", "ROLE_ADMIN"})
	@RequestMapping(method = RequestMethod.DELETE, value="/{id}/items/{itemId}/approve")
	public ResponseEntity<LineItemResource> rejectLineItem(@PathVariable Long id, @PathVariable Long itemId) throws Exception{
		LineItem li = phrManager.rejectItemInPHR(itemId);
		LineItemResource lir = lineItemAssembler.toResource(li);
		return new ResponseEntity<LineItemResource>(lir, HttpStatus.OK);
	}
	
//	@Secured({"ROLE_WE", "ROLE_ADMIN"})
	@RequestMapping(method = RequestMethod.POST, value="/{id}/items/{itemId}/approve")
	public ResponseEntity<LineItemResource> approveLineItem(@PathVariable Long id, @PathVariable Long itemId) throws Exception{
		LineItem li = phrManager.approveItemInPHR(itemId);
		LineItemResource lir = lineItemAssembler.toResource(li);
		return new ResponseEntity<LineItemResource>(lir, HttpStatus.OK);
	}
	
//	@Secured({"ROLE_WE", "ROLE_ADMIN"})
	@RequestMapping(method = RequestMethod.POST, value="/{id}/items/{itemId}")
	public ResponseEntity<LineItemResource> modifyLineItem(@PathVariable Long id, @PathVariable Long itemId, @RequestBody LineItemResource item) throws Exception{
		LineItem li = phrManager.modifyItemInPHR(itemId, item);
		LineItemResource lir = lineItemAssembler.toResource(li);
		return new ResponseEntity<LineItemResource>(lir, HttpStatus.OK);
	}
	
//	@Secured({"ROLE_SE", "ROLE_ADMIN"}
	@RequestMapping(method = RequestMethod.POST, value="/{id}/items/{li.id}/update")
	public ResponseEntity<UpdatePHRResource> createUpdatePHR(@PathVariable Long id, @PathVariable("li.id") Long liId, @RequestBody UpdatePHRResource updatePHRR) throws Exception {
		UpdatePHR updatePHR = phrManager.extendPHR(id, liId, updatePHRR);
		UpdatePHRResource phrr = updatePHRAssembler.toResource(updatePHR);
		return new ResponseEntity<UpdatePHRResource>(phrr, HttpStatus.CREATED);
	}
	
//	@RequestMapping(method = RequestMethod.POST, value="/{id}/updates")
//	public ResponseEntity<UpdatePHRResource> createUpdatePHR(@PathVariable Long id,@RequestBody UpdatePHRResource updatePHRR) throws PlantNotAvailableException, WrongArgumentException {
//		UpdatePHR updatePHR = phrManager.extendPHR(id, updatePHRR);
//		UpdatePHRResource phrr = updatePHRAssembler.toResource(updatePHR);
//		return new ResponseEntity<UpdatePHRResource>(phrr, HttpStatus.CREATED);
//	}
//
//	@RequestMapping(method = RequestMethod.GET, value="/{id}/updates/{up.id}")
//	public ResponseEntity<UpdatePHRResource> getUpdatePHR(@PathVariable Long id, @PathVariable("up.id") Long upId){
//		UpdatePHR updatePHR = updatePHRRepo.findOne(upId);
//		UpdatePHRResource phrr = updatePHRAssembler.toResource(updatePHR);
//		return new ResponseEntity<UpdatePHRResource>(phrr, HttpStatus.OK);
//	}
//
//	@RequestMapping(method = RequestMethod.POST, value="/{id}/updates/{up.id}/approve")
//	public ResponseEntity<UpdatePHRResource> approveUpdatePHR(@PathVariable Long id, @PathVariable("up.id") Long upId) throws PlantNotAvailableException {
//		UpdatePHR updatePHR = phrManager.approveUpdatePHR(upId);
//		UpdatePHRResource updatePHRR = updatePHRAssembler.toResource(updatePHR);
//		return new ResponseEntity<UpdatePHRResource>(updatePHRR, HttpStatus.OK);
//	}
//
//	@RequestMapping(method = RequestMethod.DELETE, value="/{id}/updates/{up.id}/approve")
//	public ResponseEntity<UpdatePHRResource> rejectUpdatePHR(@PathVariable Long id, @PathVariable("up.id") Long upId){
//		UpdatePHR updatePHR = phrManager.rejectUpdatePHR(upId);
//		UpdatePHRResource updatePHRR = updatePHRAssembler.toResource(updatePHR);
//		return new ResponseEntity<UpdatePHRResource>(updatePHRR, HttpStatus.OK);
//	}
//
//	@RequestMapping(method = RequestMethod.POST, value="/{id}/createPO")
//	public ResponseEntity<PlantHireRequestResource> createPurchaseOrder(@PathVariable Long id) throws PlantNotAvailableException, RestClientException{
//		PlantHireRequest phr = phrManager.createPurchaseOrder(id);
//		PlantHireRequestResource phrr = phrAssembler.toResource(phr);
//		return new ResponseEntity<PlantHireRequestResource>(phrr, HttpStatus.OK);
//	}

	@RequestMapping(value="/{id}/items")
	public ResponseEntity<List<LineItemResource>> getItems(@PathVariable Long id){
		return new ResponseEntity<>(lineItemAssembler.toResources(phrManager.getPlantHireRequest(id).getItems()),HttpStatus.OK);
	}

	@RequestMapping(value="/{id}/items/{itemId}")
	public ResponseEntity<LineItemResource> getItems(@PathVariable Long id, @PathVariable Long itemId){
		return new ResponseEntity<>(lineItemAssembler.toResource(lineItemRepo.getOne(itemId)),HttpStatus.OK);
	}

    @ExceptionHandler({CannotCancelException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    public void handleCannotCancelException(){
    }

    @ExceptionHandler({WrongArgumentException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handleWrongArgumentException(){
    }

}
