package demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import demo.integration.dto.AuthorityResource;
import demo.integration.dto.assemblers.AuthorityResourceAssembler;
import demo.integration.dto.assemblers.UserAssembler;
import demo.services.UserManager;


@RestController
@RequestMapping("/rest/users")
public class AuthorityController {

	@Autowired
	UserManager manager;
	
	@Autowired
	UserAssembler assembler;
	
	@Autowired
	AuthorityResourceAssembler authorityAssembler;
	
	@Secured({"ROLE_ADMIN"})
	@RequestMapping("/{id}/authorities")
	public ResponseEntity<List<AuthorityResource>> getUserAuthorities(@PathVariable String id){
		return new ResponseEntity<List<AuthorityResource>>(authorityAssembler.toResources(manager.getUserAuthorities(id)),HttpStatus.OK);
	}
	
	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/{id}/authorities", method = RequestMethod.POST)
	public ResponseEntity<AuthorityResource> addAuthority(@PathVariable String id, @RequestBody AuthorityResource authorityRes){
		return new ResponseEntity<AuthorityResource>(authorityAssembler.toResource(manager.addUserAuthority(id, authorityRes)),HttpStatus.CREATED);
	}
	
	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value = "/{id}/authorities/{authId}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public void deleteAuthority(@PathVariable String id, @PathVariable Long authId){
		manager.deleteUserAuthority(id, authId);
	}
	
}
