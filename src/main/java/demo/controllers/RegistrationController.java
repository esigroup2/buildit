package demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import demo.integration.dto.assemblers.UserAssembler;
import demo.integration.dto.UserRegistreResource;
import demo.integration.dto.UserResource;
import demo.services.UserManager;

@RequestMapping("/registration")
@RestController
public class RegistrationController {

	@Autowired
	UserManager manager;
	
	@Autowired
	UserAssembler assembler;
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<UserResource> register(@RequestBody UserRegistreResource userRes){
		return new ResponseEntity<UserResource>(assembler.toResource(manager.createUser(userRes)),HttpStatus.CREATED);
	}
	
}
