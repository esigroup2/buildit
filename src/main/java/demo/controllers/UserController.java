package demo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.integration.dto.assemblers.UserAssembler;
import demo.integration.dto.UserResource;
import demo.services.UserManager;

@RestController
@RequestMapping("/rest/users")
public class UserController {

	@Autowired
	UserManager manager;
	
	@Autowired
	UserAssembler assembler;
	
	@RequestMapping
	@Secured({"ROLE_ADMIN"})
	public ResponseEntity<List<UserResource>> getAll(){
		return new ResponseEntity<List<UserResource>>(assembler.toResources(manager.getAll()),HttpStatus.OK);
	}
	
	
	@RequestMapping("/{id}")
	@Secured({"ROLE_ADMIN"})
	public ResponseEntity<UserResource> getUser(@PathVariable String id){
		return new ResponseEntity<UserResource>(assembler.toResource(manager.getUser(id)),HttpStatus.OK);
	}
	
}
