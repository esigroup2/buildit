package demo.models;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Entity
@Data
@XmlRootElement
public class PlantHireRequest {
	@Id
	@GeneratedValue
	Long id;
	
	@OneToOne
	User siteEngineer;

	@OneToOne
	User workEngineer;
	
	@Enumerated(EnumType.STRING)
	PHRStatus status;

	@OneToMany(mappedBy = "plantHireRequest", cascade = CascadeType.ALL)
	List<LineItem> items;
}
