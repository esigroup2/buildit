package demo.models;

import lombok.Data;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Created by ostap_000 on 01/12/2014.
 */

@Entity
@Data
@XmlRootElement
public class LineItem {

    @Id
    @GeneratedValue
    Long id;

    @ManyToOne
    Supplier supplier;

    String plantRef;
    String purchaseOrderRef;

    @Temporal(TemporalType.DATE)
    Date startDate;

    @Temporal(TemporalType.DATE)
    Date endDate;

    LineItemStatus status;
    Float price;

    @ManyToOne
    PlantHireRequest plantHireRequest;
}
