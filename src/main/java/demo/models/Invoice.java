package demo.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Entity
@Data
@XmlRootElement
public class Invoice {
	@Id
	@GeneratedValue
	Long id;
	
	String purchaseOrderRef;
	Float total;
	@Temporal(TemporalType.DATE)
	Date dueDate;
	
	@Enumerated(EnumType.STRING)
	InvoiceStatus status;

	String providerId;
}
