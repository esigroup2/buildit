package demo.models;

public enum LineItemStatus {
	PENDING, APPROVED, REJECTED, REJECTED_BY_SUPPLIER, PENDING_UPDATE, CLOSED
}
