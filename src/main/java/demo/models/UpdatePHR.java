package demo.models;

import java.util.Date;


import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class UpdatePHR {

	@Id
	@GeneratedValue
	Long id;
	
	@Temporal(TemporalType.DATE)
	Date startDate;
	
	@Temporal(TemporalType.DATE)
	Date endDate;

	@OneToOne(cascade = {CascadeType.DETACH})
	LineItem item;
	
	@Enumerated(EnumType.STRING)
	PHRStatus status;
	
}
