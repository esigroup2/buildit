package demo.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by ostap_000 on 02/12/2014.
 */

@Entity
@Data
public class Supplier {

    @Id
    @GeneratedValue
    Long id;

    String name;

    String host;

    String port;

    String email;
}
