package demo.models.repositories;

import demo.models.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by ostap_000 on 02/12/2014.
 */

@Repository
public interface SupplierRepository extends JpaRepository<Supplier, Long> {
}
