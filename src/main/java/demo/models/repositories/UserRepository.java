package demo.models.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import demo.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, String>{

}
