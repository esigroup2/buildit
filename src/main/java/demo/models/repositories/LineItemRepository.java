package demo.models.repositories;

import demo.models.LineItem;
import demo.models.LineItemStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by ostap_000 on 01/12/2014.
 */
public interface LineItemRepository extends JpaRepository<LineItem,Long>{

    List<LineItem> findByStatus(LineItemStatus status);

    @Query("SELECT li FROM LineItem li WHERE li.purchaseOrderRef like :purchaseOrderRef")
    LineItem getByPurchaseOrderRef(@Param("purchaseOrderRef") String purchaseOrderRef);

}
