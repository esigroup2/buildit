package demo.models.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import demo.models.UpdatePHR;

@Repository
public interface UpdatePHRRepository extends JpaRepository<UpdatePHR, Long> {


}
