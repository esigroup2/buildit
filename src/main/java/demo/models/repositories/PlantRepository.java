package demo.models.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import demo.models.Plant;

@Repository
public interface PlantRepository extends JpaRepository<Plant, Long> {
	
	public List<Plant> findByNameLike(String name);
	
//    @Query("SELECT p from Plant p WHERE LOWER(p.name) LIKE LOWER(:name) AND (SELECT COUNT(phr) FROM PlantHireRequest phr "
//    		+ "WHERE phr.plant.id = p.id AND ((phr.startDate <= :to AND  phr.startDate >= :from) OR (phr.endDate <= :to AND  phr.endDate >= :from))) = 0")
//    List<Plant> findAvaiableBetweenDates(@Param("name") String name, @Param("from") Date from, @Param("to") Date to);
}
