package demo.models.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import demo.models.SiteEngineer;

@Repository
public interface SiteEngineerRepository extends JpaRepository<SiteEngineer, Long>{

}
