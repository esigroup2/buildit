package demo.models;

public enum InvoiceStatus {
	PENDING, ACCEPTED, REJECTED
}
