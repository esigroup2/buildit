package demo.integration.dto;

import java.util.Date;
import java.util.List;

import demo.models.PHRStatus;
import lombok.Getter;
import lombok.Setter;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import demo.util.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class PlantHireRequestResource extends ResourceSupport {

    Long idRes;

    List<LineItemResource> items;

    PHRStatus status;

}
