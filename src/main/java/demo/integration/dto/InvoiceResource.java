package demo.integration.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import demo.util.ResourceSupport;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
@XmlRootElement
public class InvoiceResource extends ResourceSupport {

	PurchaseOrderResource purchaseOrder;
	Float total;
	@DateTimeFormat(iso = ISO.DATE)
	Date dueDate;
}
