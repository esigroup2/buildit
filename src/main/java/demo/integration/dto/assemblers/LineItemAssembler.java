package demo.integration.dto.assemblers;

import demo.controllers.PHRController;
import demo.integration.dto.LineItemResource;
import demo.integration.dto.PlantResource;
import demo.integration.dto.PurchaseOrderResource;
import demo.integration.dto.assemblers.SupplierAssembler;
import demo.models.LineItem;
import demo.models.PlantHireRequest;
import demo.util.ExtendedLink;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by ostap_000 on 01/12/2014.
 */
public class LineItemAssembler extends ResourceAssemblerSupport<LineItem, LineItemResource> {


    SupplierAssembler supplierAssembler = new SupplierAssembler();

    public LineItemAssembler() {
        super(PHRController.class, LineItemResource.class);
    }

    @Override
    public LineItemResource toResource(LineItem entity) {
        LineItemResource lineItemResource = createResourceWithId(entity.getId(),entity);


        lineItemResource.setIdRes(entity.getId());
        lineItemResource.setSupplier(supplierAssembler.toResource(entity.getSupplier()));

        if (entity.getPlantRef() != null) {
            PlantResource plantRes = new PlantResource();
            plantRes.add(new Link(entity.getPlantRef()));
            plantRes.setSupplierId(lineItemResource.getSupplier().getIdRes());
            lineItemResource.setPlant(plantRes);
        }

        if (entity.getPurchaseOrderRef() != null) {
            PurchaseOrderResource poRes = new PurchaseOrderResource();
            poRes.add(new Link(entity.getPurchaseOrderRef()));
            lineItemResource.setPurchaseOrder(poRes);
        }

        lineItemResource.setStartDate(entity.getStartDate());
        lineItemResource.setEndDate(entity.getEndDate());
        lineItemResource.setPrice(entity.getPrice());
        lineItemResource.setStatus(entity.getStatus());

        System.out.println("Line status" + entity.getStatus().toString());

        try {
            switch (entity.getStatus()) {
                case APPROVED:
                    lineItemResource.add(new ExtendedLink(linkTo(methodOn(PHRController.class).createUpdatePHR(entity.getPlantHireRequest().getId(), entity.getId(),null)).toString(), "extend", "POST"));
                    lineItemResource.add(new ExtendedLink(linkTo(methodOn(PHRController.class).cancelLineItemInRequest(entity.getPlantHireRequest().getId(),entity.getId())).toString(), "cancel", "DELETE"));
                    break;
                case PENDING:
                    lineItemResource.add(new ExtendedLink(linkTo(methodOn(PHRController.class).approveLineItem(entity.getPlantHireRequest().getId(), entity.getId())).toString(), "approve", "POST"));
                    lineItemResource.add(new ExtendedLink(linkTo(methodOn(PHRController.class).rejectLineItem(entity.getPlantHireRequest().getId(), entity.getId())).toString(), "reject", "DELETE"));
                    lineItemResource.add(new ExtendedLink(linkTo(methodOn(PHRController.class).modifyLineItem(entity.getPlantHireRequest().getId(), entity.getId(), null)).toString(), "update", "POST"));
//               res.add(new ExtendedLink(linkTo(methodOn(PHRController.class).cancelRequest(phr.getId())).toString(), "cancel", "DELETE"));
                    break;
                case REJECTED:
                    lineItemResource.add(new ExtendedLink(linkTo(methodOn(PHRController.class).modifyLineItem(entity.getPlantHireRequest().getId(), entity.getId(), null)).toString(), "update", "POST"));
                    break;
                default:
                    break;
            }
        } catch(Exception e) {}

        return lineItemResource;
    }
}
