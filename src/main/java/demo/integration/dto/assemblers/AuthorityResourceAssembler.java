package demo.integration.dto.assemblers;

import demo.integration.dto.AuthorityResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import demo.controllers.AuthorityController;
import demo.controllers.UserController;
import demo.models.Authority;
import demo.models.User;

@Service
public class AuthorityResourceAssembler extends ResourceAssemblerSupport<Authority, AuthorityResource> {

	public AuthorityResourceAssembler() {
		super(AuthorityController.class, AuthorityResource.class);
	}


	@Override
	public AuthorityResource toResource(Authority authotiry) {
		AuthorityResource authorityRes = createResourceWithId(authotiry.getId(), authotiry);
		authorityRes.setIdRes(authotiry.getId());
		authorityRes.setUsername(authotiry.getUsername().getUsername());
		authorityRes.setAuthority(authotiry.getAuthority());
		return authorityRes;
	}
}