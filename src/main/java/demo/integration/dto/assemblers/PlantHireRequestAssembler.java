package demo.integration.dto.assemblers;


import demo.integration.dto.PlantHireRequestResource;
import demo.integration.dto.assemblers.LineItemAssembler;
import demo.models.LineItem;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import demo.controllers.PHRController;
import demo.models.PlantHireRequest;
import demo.util.ExtendedLink;

import java.util.ArrayList;

public class PlantHireRequestAssembler extends ResourceAssemblerSupport<PlantHireRequest, PlantHireRequestResource>{

	LineItemAssembler lineItemAssembler = new LineItemAssembler();

	public PlantHireRequestAssembler(){
		super(PHRController.class, PlantHireRequestResource.class);
	}
	
	@Override
	public PlantHireRequestResource toResource(PlantHireRequest phr){
		PlantHireRequestResource res = createResourceWithId(phr.getId(), phr);

		res.setIdRes(phr.getId());
		res.setItems(new ArrayList<>());
		for (LineItem lineItem : phr.getItems()) {
			res.getItems().add(lineItemAssembler.toResource(lineItem));
		}

		res.setStatus(phr.getStatus());

		try {
			System.out.println("status" + phr.getStatus().toString());
			switch (phr.getStatus()) {
//			case APPROVED:
//				res.add(new ExtendedLink(linkTo(methodOn(PHRController.class).createUpdatePHR(phr.getId(),null)).toString(), "extend", "POST"));
//                res.add(new ExtendedLink(linkTo(methodOn(PHRController.class).cancelRequest(phr.getId())).toString(), "cancel", "DELETE"));
//				break;
			case PENDING:
				res.add(new ExtendedLink(linkTo(methodOn(PHRController.class).approveRequest(phr.getId())).toString(), "approve", "POST"));
				res.add(new ExtendedLink(linkTo(methodOn(PHRController.class).rejectRequest(phr.getId())).toString(), "reject", "DELETE"));
				res.add(new ExtendedLink(linkTo(methodOn(PHRController.class).updateRequest(phr.getId(), null)).toString(), "update", "POST"));
//               res.add(new ExtendedLink(linkTo(methodOn(PHRController.class).cancelRequest(phr.getId())).toString(), "cancel", "DELETE"));
				break;
			case REJECTED:
				res.add(new ExtendedLink(linkTo(methodOn(PHRController.class).updateRequest(phr.getId(), null)).toString(), "update", "POST"));
				break;
			default:
				break;
			}
		} catch(Exception e) {}


		return res;
	}
}
