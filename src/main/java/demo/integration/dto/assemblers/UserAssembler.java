package demo.integration.dto.assemblers;

import demo.integration.dto.UserResource;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import demo.controllers.UserController;
import demo.models.UpdatePHR;
import demo.models.User;

@Service
public class UserAssembler extends ResourceAssemblerSupport<User, UserResource> {

	public UserAssembler() {
		super(UserController.class, UserResource.class);
	}

	@Override
	public UserResource toResource(User user) {
		UserResource userRes = createResourceWithId(user.getUsername(), user);
		userRes.setUsername(user.getUsername());
		userRes.setEnabled(user.isEnabled());
		return userRes;
	}
}