package demo.integration.dto.assemblers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import demo.integration.dto.InvoiceResource;
import demo.integration.dto.PurchaseOrderResource;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import demo.controllers.InvoiceController;
import demo.models.Invoice;
import demo.util.ExtendedLink;

public class InvoiceAssembler extends ResourceAssemblerSupport<Invoice, InvoiceResource>{
	
	public InvoiceAssembler(){
		super(InvoiceController.class, InvoiceResource.class);
	}
	
	@Override
	public InvoiceResource toResource(Invoice inv){
		InvoiceResource res = createResourceWithId(inv.getId(), inv);
		res.setTotal(inv.getTotal());
		res.setDueDate(inv.getDueDate());
		
		if (inv.getPurchaseOrderRef() != null) {
			PurchaseOrderResource poRes = new PurchaseOrderResource();
			poRes.add(new Link(inv.getPurchaseOrderRef()));
			res.setPurchaseOrder(poRes);
		}

		try {
			System.out.println("status " + inv.getStatus().toString());
			switch (inv.getStatus()) {
			case ACCEPTED:
				break;
			case PENDING:
				res.add(new ExtendedLink(linkTo(methodOn(InvoiceController.class).acceptInvoice(inv.getId())).toString(), "accept", "POST"));
				res.add(new ExtendedLink(linkTo(methodOn(InvoiceController.class).rejectInvoice(inv.getId())).toString(), "reject", "DELETE"));
				break;
			case REJECTED:
				break;
			default:
				break;
			}
		} catch(Exception e) {}


		return res;
	}
}
