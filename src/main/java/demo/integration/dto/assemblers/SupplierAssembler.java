package demo.integration.dto.assemblers;

import demo.controllers.PHRController;
import demo.integration.dto.LineItemResource;
import demo.integration.dto.SupplierResource;
import demo.models.LineItem;
import demo.models.Supplier;
import demo.models.repositories.SupplierRepository;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by ostap_000 on 03/12/2014.
 */
public class SupplierAssembler extends ResourceAssemblerSupport<Supplier, SupplierResource> {


    public SupplierAssembler() {
        super(PHRController.class, SupplierResource.class);
    }

    @Override
    public SupplierResource toResource(Supplier entity) {
        SupplierResource supplierResource = new SupplierResource();
        supplierResource.setName(entity.getName());
        supplierResource.setIdRes(entity.getId());
        return supplierResource;
    }
}
