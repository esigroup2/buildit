package demo.integration.dto.assemblers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import demo.integration.dto.UpdatePHRResource;
import demo.services.PlantNotAvailableException;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import demo.controllers.PHRController;
import demo.models.UpdatePHR;
import demo.util.ExtendedLink;

public class UpdatePHRAssembler  extends ResourceAssemblerSupport<UpdatePHR, UpdatePHRResource>{
	
	public UpdatePHRAssembler(){
		super(PHRController.class, UpdatePHRResource.class);
	}

	@Override
	public UpdatePHRResource toResource(UpdatePHR updatePHR) {
		UpdatePHRResource updatePHRResource =  createResourceWithId(updatePHR.getId(), updatePHR);
		updatePHRResource.setStartDate(updatePHR.getStartDate());
		updatePHRResource.setEndDate(updatePHR.getEndDate());
//        try {
//            switch (updatePHR.getStatus()) {
//
//                case PENDING:
//                    updatePHRResource.add(new ExtendedLink(linkTo(methodOn(PHRController.class).approveUpdatePHR(updatePHR.getPhr().getId(), updatePHR.getId())).toString(), "approve", "POST"));
//                    updatePHRResource.add(new ExtendedLink(linkTo(methodOn(PHRController.class).rejectUpdatePHR(updatePHR.getPhr().getId(), updatePHR.getId())).toString(), "reject", "DELETE"));
//                    break;
//
//                default:
//                    break;
//            }
//
//        } catch (PlantNotAvailableException e) {
//        }
		
		return updatePHRResource;
	}
	
}
