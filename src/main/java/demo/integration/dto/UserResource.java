package demo.integration.dto;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;
import demo.util.ResourceSupport;

@Getter
@Setter
@XmlRootElement(name = "user")
public class UserResource  extends ResourceSupport{

    String username;
    boolean enabled;
	
}
