package demo.integration.dto;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;
import demo.util.ResourceSupport;

@Getter
@Setter
@XmlRootElement(name = "authority")
public class AuthorityResource extends ResourceSupport{

	Long idRes;
    String username;
    String authority;
    
}
