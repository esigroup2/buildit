package demo.integration.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import demo.util.ResourceSupport;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by ostap_000 on 03/12/2014.
 */

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SupplierResource extends ResourceSupport {

    Long idRes;
    String name;

}
