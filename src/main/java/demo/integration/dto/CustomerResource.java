package demo.integration.dto;

import javax.xml.bind.annotation.XmlRootElement;

import demo.util.ResourceSupport;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name = "customer")
public class CustomerResource  extends ResourceSupport{

    Long idRes;
    String name;
    String vatNumber;
	
}
