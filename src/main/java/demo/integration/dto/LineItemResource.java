package demo.integration.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import demo.models.LineItemStatus;
import demo.util.ResourceSupport;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by ostap_000 on 01/12/2014.
 */

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LineItemResource extends ResourceSupport {

    Long idRes;

    PlantResource plant;

    PurchaseOrderResource purchaseOrder;

    SupplierResource supplier;

    @DateTimeFormat(iso= DateTimeFormat.ISO.DATE)
    Date startDate;

    @DateTimeFormat(iso= DateTimeFormat.ISO.DATE)
    Date endDate;

    Float price;

    LineItemStatus status;

}
