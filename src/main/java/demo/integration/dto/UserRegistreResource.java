package demo.integration.dto;

import javax.xml.bind.annotation.XmlRootElement;

import demo.util.ResourceSupport;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name = "user")
public class UserRegistreResource extends ResourceSupport{

    String username;
    String password;
	
}