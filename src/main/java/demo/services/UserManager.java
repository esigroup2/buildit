package demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.integration.dto.AuthorityResource;
import demo.integration.dto.UserRegistreResource;
import demo.models.Authority;
import demo.models.User;
import demo.models.repositories.AuthorityRepository;
import demo.models.repositories.UserRepository;

@Service
public class UserManager {

	@Autowired
	UserRepository repo;

	@Autowired
	AuthorityRepository authorityRepo;

	public List<User> getAll() {
		return repo.findAll();
	}

	public User getUser(String id) {
		return repo.findOne(id);
	}

	public User createUser(UserRegistreResource userRes) {
		User user = new User();
		user.setUsername(userRes.getUsername());
		user.setPassword(userRes.getPassword());
		user.setEnabled(true);

		user = repo.saveAndFlush(user);

		Authority authority = new Authority();
		authority.setUsername(user);
		authority.setAuthority("ROLE_ADMIN");
		authorityRepo.saveAndFlush(authority);

		return user;
	}

	public Authority addUserAuthority(String username, AuthorityResource authorityRes) {
		Authority authority = new Authority();
		authority.setUsername(getUser(username));
		authority.setAuthority(authorityRes.getAuthority());
		authority = authorityRepo.saveAndFlush(authority);
		return authority;
	}

	public List<Authority> getUserAuthorities(String username) {
		return authorityRepo.findByUsername(username);
	}

	public void deleteUserAuthority(String username, Long id) {
		authorityRepo.delete(id);
	}

}
