package demo.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.xml.sax.helpers.DefaultHandler; 
import org.xml.sax.*; 

import demo.models.Invoice;

public class SAXHandler extends DefaultHandler{
	Invoice inv = new Invoice();
	String thisElement = "";
	
	public Invoice getResult(){
		return inv;
	}
	 
	@Override 
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException { 
	  thisElement = qName;  
	} 
	 
	@Override 
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException { 
	  thisElement = ""; 
	} 
	 
	@Override 
	public void characters(char[] ch, int start, int length) throws SAXException { 
	  if (thisElement.equals("id")) { 
	     inv.setId(new Long(new String(ch, start, length))); 
	  } 
	  if (thisElement.equals("purchaseOrder")) { 
	     inv.setPurchaseOrderRef(new String(ch, start, length)); 
	  } 
	  if (thisElement.equals("total")) { 
	     inv.setTotal(new Float(new String(ch, start, length))); 
	  } 
	  if (thisElement.equalsIgnoreCase("dueDate")) { 
		 String dateInString = (new String(ch, start, length)); 
	     SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd");
	     try {
	    	 Date date = sdf.parse(dateInString);
	    	 inv.setDueDate(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	  } 
	} 
}
