package demo.services.mail;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import lombok.Data;

@Entity
@Data
@XmlRootElement(name="invoice")
@XmlAccessorType(XmlAccessType.FIELD)
public class T3Invoice {

    @Id
    @GeneratedValue
    @XmlTransient
    Long id;

    @XmlElement(name = "idRes")
    Long idRes;

    @XmlElement(name = "date")
    @Temporal(TemporalType.DATE)
    Date date;

    Float total;

    String purchaseOrderRef;

    InvoiceStatus invoiceStatus;

    Boolean isRejected = false;

    public enum InvoiceStatus {
        PAID, NOT_PAID
    }
}
