package demo.services.mail;

/**
 * Created by ostap_000 on 17/12/2014.
 */
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Entity
@Data
@XmlRootElement(name="remittanceAdvice")
public class T4RemittanceAdvice {

    @Id
    @GeneratedValue
    Long id;

    @Temporal(TemporalType.DATE)
    Date datePayed;

    Float amountPayed;

    String purchaseOrderRef;
}
