package demo.services.mail;

/**
 * Created by ostap_000 on 17/12/2014.
 */
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Entity
@Data
@XmlRootElement(name = "rm")
public class T3RemittanceAdvice {
    @Id
    @GeneratedValue
    Long id;
    Long invoiceId;
    Date paymentDate;
}

