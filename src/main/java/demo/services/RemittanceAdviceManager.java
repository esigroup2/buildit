package demo.services;

import java.io.File;
import java.util.Date;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import demo.models.LineItem;
import demo.models.Supplier;
import demo.models.repositories.LineItemRepository;
import demo.services.mail.T3RemittanceAdvice;
import demo.services.mail.T4RemittanceAdvice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import demo.integration.dto.assemblers.InvoiceAssembler;
import demo.integration.dto.PurchaseOrderResource;
import demo.integration.dto.RemittanceAdviceResource;
import demo.models.Invoice;
import demo.services.rentit.RentalService;

@Service
public class RemittanceAdviceManager {
	
	@Autowired
    RentalService rentitProxy;
	
	@Autowired
	JavaMailSenderImpl mailSender;
	
	private static String FILE_NAME = "Remittance.xml";

	@Autowired
	LineItemRepository lineItemRepository;
	
	private InvoiceAssembler invoiceAssembler = new InvoiceAssembler();

    public MimeMessage sendRemittanceFast(Invoice invoice) throws MessagingException {
		int start = invoice.getPurchaseOrderRef().indexOf("/rest/pos");
		String line = invoice.getPurchaseOrderRef().substring(start);
		LineItem lineItem = lineItemRepository.getByPurchaseOrderRef("%" + line + "%");

    	return createMessage(invoice, lineItem.getSupplier());
    }
    
    public void sendRemmitance(Invoice invoice) throws MessagingException{
		int start = invoice.getPurchaseOrderRef().indexOf("/rest/pos");
		String line = invoice.getPurchaseOrderRef().substring(start);
		LineItem lineItem = lineItemRepository.getByPurchaseOrderRef("%" + line + "%");

    	sendEmail(invoice, lineItem.getSupplier());
    }
    
    private MimeMessage createMessage(Invoice invoice, Supplier supplier) throws AddressException, MessagingException{
		String FILE_NAME = "";
		if (supplier.getId() == 1) {
			FILE_NAME = "Remittance.xml";
			RemittanceAdviceResource ra = makeAdvice(invoice,supplier);
			try {
				JAXBContext context = JAXBContext.newInstance(RemittanceAdviceResource.class);
				Marshaller m = context.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
				m.marshal(ra, new File(FILE_NAME));
			} catch (JAXBException e) {
				e.printStackTrace();
			}

		}

		if (supplier.getId() == 2) {
			FILE_NAME = "rm-advice.xml";
			T3RemittanceAdvice ra = new T3RemittanceAdvice();
			ra.setInvoiceId(Long.parseLong(invoice.getProviderId()));
			ra.setPaymentDate(new Date());
			try {
				JAXBContext context = JAXBContext.newInstance(T3RemittanceAdvice.class);
				Marshaller m = context.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
				m.marshal(ra, new File(FILE_NAME));
			} catch (JAXBException e) {
				e.printStackTrace();
			}

		}

		if (supplier.getId() == 3) {
			FILE_NAME = "remittanceAdvice.xml";
			T4RemittanceAdvice ra = new T4RemittanceAdvice();
			ra.setPurchaseOrderRef(invoice.getPurchaseOrderRef());
			ra.setAmountPayed(invoice.getTotal());
			ra.setDatePayed(new Date());
			try {
				JAXBContext context = JAXBContext.newInstance(T4RemittanceAdvice.class);
				Marshaller m = context.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
				m.marshal(ra, new File(FILE_NAME));
			} catch (JAXBException e) {
				e.printStackTrace();
			}

		}


    	JavaMailSenderImpl sender = new JavaMailSenderImpl();
    	MimeMessage message = sender.createMimeMessage();

    	message.addRecipient(Message.RecipientType.TO,
                new InternetAddress(supplier.getEmail()));
    	message.setSubject("REMITANCE");
    	
    	Multipart multipart = new MimeMultipart();
        BodyPart messageBodyPart = new MimeBodyPart();
        DataSource source = new FileDataSource(FILE_NAME);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(FILE_NAME);
        messageBodyPart.setHeader("Content-Type", "application/xml");      
        multipart.addBodyPart(messageBodyPart);
        
        message.setContent(multipart,"application/xml");
        return message;
    }
    
    private void sendEmail(Invoice invoice, Supplier supplier) throws AddressException, MessagingException{

    	MimeMessage message = createMessage(invoice, supplier);
    	mailSender.send(message);
    }
    
    public RemittanceAdviceResource makeAdvice(Invoice invoice, Supplier supplier){
    	PurchaseOrderResource por = rentitProxy.getPurchaseOrder(supplier,invoice.getPurchaseOrderRef());
    	RemittanceAdviceResource ra = new RemittanceAdviceResource();
    	ra.setPurchaseOrderId(por.getIdRes());
    	ra.setTotal(invoice.getTotal());
    	return ra;
    }
}
