package demo.services;

import demo.exceptions.CannotCancelException;
import demo.exceptions.CannotFindItemInPHRException;
import demo.exceptions.WrongArgumentException;
import demo.integration.dto.*;
import demo.models.LineItem;
import demo.models.repositories.LineItemRepository;
import demo.models.repositories.SupplierRepository;
import demo.util.Utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import demo.models.LineItemStatus;
import demo.models.PHRStatus;
import demo.models.PlantHireRequest;
import demo.models.UpdatePHR;
import demo.models.repositories.PlantHireRequestRepository;
import demo.models.repositories.UpdatePHRRepository;
import demo.services.rentit.RentalService;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.*;

@Service
public class PlantHireRequestManager {
	
    @Autowired
    @Qualifier("plantHireRequestRepository")
    PlantHireRequestRepository phrRepository;
    
    @Autowired
    UpdatePHRRepository updatePHRRepo;
    
    @Autowired
    RentalService rentitProxy;

	@Autowired
	LineItemRepository lineItemRepo;

	@Autowired
	SupplierRepository supplierRepo;
    
	public PlantHireRequest createPurchaseOrder(Long id) throws PlantNotAvailableException, RestClientException {
		PlantHireRequest phr = phrRepository.findOne(id);

		for (LineItem lineItem : phr.getItems()) {
			PurchaseOrderResource po = rentitProxy.createPurchaseOrder(lineItem);
			System.out.println(po.getId().getHref());
			lineItem.setPurchaseOrderRef(po.getId().getHref());
			lineItemRepo.saveAndFlush(lineItem);
		}

        phr = phrRepository.saveAndFlush(phr);
		return phr;
	}

	public PlantHireRequest createPlantHireRequest(PlantHireRequest phr) {
		phr.setStatus(PHRStatus.PENDING);
		for (LineItem lineItem : phr.getItems()) {
			lineItem.setStatus(LineItemStatus.PENDING);
			lineItem.setPlantHireRequest(phr);
		}
		phr = phrRepository.saveAndFlush(phr);
		return phr;
	}	
	
	public PlantHireRequest approvePlantHireRequest(Long id) throws RestClientException, PlantNotAvailableException {

		PlantHireRequest phr = phrRepository.findOne(id);
		phr.setStatus(PHRStatus.APPROVED);
		phr = phrRepository.saveAndFlush(phr);

		phr = createPurchaseOrder(id);
		return phr;
	}

	 public void cancelItemInPHR(Long id, Long itemId) throws CannotCancelException, CannotFindItemInPHRException {
		 PlantHireRequest phr = phrRepository.findOne(id);
		 List<LineItem> items = phr.getItems();
		 LineItem cancelItem = null;
		 for (LineItem lineItem : phr.getItems()) {
			 if (lineItem.getId() == itemId){
				 cancelItem = lineItem;
			 }
		 }
		 if (cancelItem == null) throw new CannotFindItemInPHRException();
		 
		 int period = Utils.getPeriod(new Date(), cancelItem.getStartDate());
		 
		 if (period <= 0) throw new CannotCancelException();
		 
		 if (cancelItem.getStatus() == LineItemStatus.APPROVED){
           PurchaseOrderResource por = rentitProxy.getPurchaseOrder(cancelItem.getSupplier(), cancelItem.getPurchaseOrderRef());
           rentitProxy.cancelPO(cancelItem.getSupplier(), por);
		 }

		 items.remove(cancelItem);

		 lineItemRepo.delete(cancelItem);
		 if (items.size()==0)
			 phrRepository.delete(phr);
		 else {
			 phr.setItems(items);
			 phrRepository.saveAndFlush(phr);
		 }
	 }
	 
	public LineItem rejectItemInPHR(Long lineId) {
		LineItem li = lineItemRepo.findOne(lineId);
		li.setStatus(LineItemStatus.REJECTED);
		lineItemRepo.saveAndFlush(li);
		return li;
	}
	 
	public LineItem approveItemInPHR(Long lineId) throws RestClientException, PlantNotAvailableException {
		LineItem li = lineItemRepo.findOne(lineId);
		
		PurchaseOrderResource po = rentitProxy.createPurchaseOrder(li);

		li.setStatus(LineItemStatus.APPROVED);
		li.setPurchaseOrderRef(po.getId().getHref());
		lineItemRepo.saveAndFlush(li);
		return li;
	}
	 
	public LineItem modifyItemInPHR(Long lineId, LineItemResource lir) throws RestClientException, PlantNotAvailableException{
		LineItem li = lineItemRepo.findOne(lineId);
		li.setStatus(LineItemStatus.PENDING);
		li.setStartDate(lir.getStartDate());
		li.setEndDate(lir.getEndDate());
		float price = lir.getPlant().getPrice();
		li.setPrice(Utils.getPeriod(lir.getStartDate(), lir.getEndDate()) * price);
		li.setPlantRef(lir.getPlant().getId().getHref());
		li.setSupplier(supplierRepo.getOne(lir.getPlant().getSupplierId()));

		lineItemRepo.saveAndFlush(li);
		return li;
	}
	
	public PlantHireRequest rejectPlantHireRequest(Long id) {
		PlantHireRequest phr = phrRepository.findOne(id);
		phr.setStatus(PHRStatus.REJECTED);
		phr = phrRepository.saveAndFlush(phr);
		return phr;
	}
	
	public UpdatePHR extendPHR(Long id, Long liId, UpdatePHRResource updatePHRR) throws WrongArgumentException, CannotFindItemInPHRException, RestClientException, PlantNotAvailableException {
        if (updatePHRR.getStartDate().after(updatePHRR.getEndDate())) throw new WrongArgumentException();

		PlantHireRequest phr = phrRepository.findOne(id);
		LineItem lineItem = null;
		for (LineItem item : phr.getItems()) {
			if (item.getId() == liId){
				lineItem = item;
			}
		}
		if (lineItem == null) throw new CannotFindItemInPHRException();

		lineItem.setStatus(LineItemStatus.PENDING_UPDATE);
		lineItem = lineItemRepo.saveAndFlush(lineItem);

		UpdatePHR updatePHR = new UpdatePHR();
		updatePHR.setItem(lineItem);
		updatePHR.setStartDate(Utils.addDays(lineItem.getEndDate(),1));
		updatePHR.setEndDate(updatePHRR.getEndDate());
		updatePHR.setStatus(PHRStatus.PENDING);
		updatePHR = updatePHRRepo.saveAndFlush(updatePHR);
		updatePHR = approveUpdatePHR(updatePHR.getId());

		return updatePHR;
	}

	public UpdatePHR approveUpdatePHR(Long upId) throws PlantNotAvailableException {
		UpdatePHR updatePHR = updatePHRRepo.findOne(upId);

		LineItem lineItem = updatePHR.getItem();
		lineItem.setStartDate(updatePHR.getStartDate());
		lineItem.setEndDate(updatePHR.getEndDate());

		updatePHR = updatePHRRepo.saveAndFlush(updatePHR);
		lineItem = lineItemRepo.saveAndFlush(lineItem);

        PurchaseOrderResource por = rentitProxy.getPurchaseOrder(lineItem.getSupplier(), lineItem.getPurchaseOrderRef());
		por.setStartDate(updatePHR.getStartDate());
		por.setEndDate(updatePHR.getEndDate());

		por = rentitProxy.updatePurchaseOrder(lineItem.getSupplier(), por.getIdRes(), por);

		updatePHR.setStatus(PHRStatus.APPROVED);
		lineItem.setStatus(LineItemStatus.APPROVED);

		updatePHR = updatePHRRepo.saveAndFlush(updatePHR);
		lineItem = lineItemRepo.saveAndFlush(lineItem);
		return updatePHR;
	}

	public UpdatePHR rejectUpdatePHR(Long upId) {
		UpdatePHR updatePHR = updatePHRRepo.findOne(upId);
		updatePHR.setStatus(PHRStatus.REJECTED);

		LineItem lineItem = updatePHR.getItem();
		lineItem.setStartDate(updatePHR.getStartDate());
		lineItem.setEndDate(updatePHR.getEndDate());
		lineItem.setStatus(LineItemStatus.APPROVED);

		updatePHR = updatePHRRepo.saveAndFlush(updatePHR);
		lineItem = lineItemRepo.saveAndFlush(lineItem);
		return updatePHR;
	}


	public PlantHireRequest updatePlantHireRequest(Long id, PlantHireRequestResource updatedPhr) throws RestClientException, PlantNotAvailableException, WrongArgumentException {
		PlantHireRequest phr = phrRepository.findOne(id);

		List<LineItem> lineItems = new ArrayList<>();

		for (LineItemResource lineItemResource : updatedPhr.getItems()) {

			if (lineItemResource.getStartDate().after(lineItemResource.getEndDate())) throw new WrongArgumentException();

			LineItem item = null;
			if (lineItemResource.getIdRes() != null){
				item = lineItemRepo.findOne(lineItemResource.getIdRes());
			}else{
				item = new LineItem();
				item.setPlantRef(lineItemResource.getPlant().getId().getHref());
				item.setStartDate(lineItemResource.getStartDate());
				item.setEndDate(lineItemResource.getEndDate());
				item.setSupplier(supplierRepo.getOne(lineItemResource.getPlant().getSupplierId()));

				float price = lineItemResource.getPlant().getPrice();
				item.setPrice(Utils.getPeriod(lineItemResource.getStartDate(), lineItemResource.getEndDate()) * price);
			}

			item = lineItemRepo.saveAndFlush(item);
			lineItems.add(item);
		}


		phr.setItems(lineItems);
		phr.setStatus(PHRStatus.PENDING);
		phr = phrRepository.saveAndFlush(phr);
		return phr;
	}

	public PlantHireRequest getPlantHireRequest(Long id) {
		return phrRepository.findOne(id);
	}
	
}
