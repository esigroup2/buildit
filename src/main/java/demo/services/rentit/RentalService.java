package demo.services.rentit;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import demo.models.LineItem;
import demo.models.Supplier;
import demo.models.repositories.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import demo.integration.dto.PlantResource;
import demo.integration.dto.PurchaseOrderResource;
import demo.services.PlantNotAvailableException;


@Service
public class RentalService {

	@Autowired
	RentalServiceFactory serviceFactory;

	@Autowired
	SupplierRepository supplierRepo;

	@Autowired
	RestTemplate restTemplate;

    public List<PlantResource> findAvailablePlants(String plantName, Date startDate, Date endDate) {
		List<Supplier> suppliers = supplierRepo.findAll();
		List<PlantResource> allPlants = new ArrayList<>();
		for (Supplier supplier : suppliers) {
			List<PlantResource> plants = serviceFactory.get(supplier).findAvailablePlants(plantName, startDate, endDate);
			plants.forEach( plant -> {
				plant.setSupplierId(supplier.getId());
				plant.setSupplierName(supplier.getName());
			});
			allPlants.addAll(plants);
		}
		return allPlants;
    }

    public List<PlantResource> getPlants() {
		Supplier supplier = supplierRepo.getOne(1L);
		List<PlantResource> plants =  serviceFactory.get(supplier).getPlants();
		plants.forEach(plant -> plant.setSupplierId(supplier.getId()));
        return plants;
    }

	public PurchaseOrderResource createPurchaseOrder(LineItem lineItem) throws RestClientException, PlantNotAvailableException {
		Supplier supplier = lineItem.getSupplier();
		return serviceFactory.get(supplier).createPurchaseOrder(lineItem);
	}


    private <T> T get(String link, Class<T> clazz){
    	ResponseEntity<T> result = restTemplate.getForEntity(link, clazz);
    	return result.getBody();
    }

    public PurchaseOrderResource getPurchaseOrder(Supplier supplier, String link){
		return serviceFactory.get(supplier).getPurchaseOrder(link);
    }
    
    public PlantResource getPlant(String link){
    	return get(link, PlantResource.class);
    }
	
	public PurchaseOrderResource updatePurchaseOrder(Supplier supplier, Long id,PurchaseOrderResource purchaseOrder) throws PlantNotAvailableException {
		return serviceFactory.get(supplier).updatePurchaseOrder(id,purchaseOrder);
	}

	public void updatePurchaseOrder(Supplier supplier, PurchaseOrderResource po) throws RestClientException, PlantNotAvailableException {
		serviceFactory.get(supplier).cancelPO(po);
	}

    public void cancelPO(Supplier supplier, PurchaseOrderResource po) {
		serviceFactory.get(supplier).cancelPO(po);
    }

	public String getStatus(LineItem item){
		PurchaseOrderResource po = getPurchaseOrder(item.getSupplier(), item.getPurchaseOrderRef());
		return getStatus(item.getSupplier(), po.getIdRes());
	}

	public String getStatus(Supplier supplier, Long id){
		return serviceFactory.get(supplier).getStatus(id);
	}
}
