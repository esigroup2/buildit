package demo.services.rentit.team4.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import demo.util.ResourceSupport;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="poUpdateRequest")
@JsonIgnoreProperties(ignoreUnknown = true)
public class T4UpdatePOResource extends ResourceSupport {

    Long updateId;
    Date endDate;
}

