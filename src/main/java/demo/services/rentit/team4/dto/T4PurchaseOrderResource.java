package demo.services.rentit.team4.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import demo.integration.dto.CustomerResource;
import demo.integration.dto.PlantResource;
import demo.util.ExtendedLink;
import demo.util.ResourceSupport;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

/**
 * Created by ostap_000 on 16/12/2014.
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class T4PurchaseOrderResource extends ResourceSupport {

    Long poId;
    T4PlantResource plant;
    T4CustomerResource customer;

    @DateTimeFormat(iso= DateTimeFormat.ISO.DATE)
    Date startDate;

    @DateTimeFormat(iso= DateTimeFormat.ISO.DATE)
    Date endDate;
    Float cost;

    List<T4UpdatePOResource> updates;
    T4InvoiceResource invoice;
    String remittanceAdvice;
    String status;
}
