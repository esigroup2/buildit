package demo.services.rentit.team4.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by ostap_000 on 16/12/2014.
 */
@Data
@XmlRootElement(name="customer")
@JsonIgnoreProperties(ignoreUnknown = true)
public class T4CustomerResource {
    Long id;
    String name;
    String vatNumber;
}