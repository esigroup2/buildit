package demo.services.rentit.team4.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import demo.util.ResourceSupport;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@XmlRootElement(name="invoice")
@JsonIgnoreProperties(ignoreUnknown = true)
public class T4InvoiceResource  extends ResourceSupport {

    Long invoiceId;

    Boolean payed;
    Date startDate;
    Date endDate;
    Date dueDate;
    Float totalCost;
    String purchaseOrderRef;
    Boolean latePayment;
}
