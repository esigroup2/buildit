package demo.services.rentit.team4.dto;

/**
 * Created by ostap_000 on 16/12/2014.
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import demo.util.ResourceSupport;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class T4PlantResource extends ResourceSupport {

    @JsonProperty("plantId")
    Long plantId;

    String name;
    String description;
    Float price;
}

