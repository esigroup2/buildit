package demo.services.rentit.team4;

import demo.integration.dto.CustomerResource;
import demo.integration.dto.PlantResource;
import demo.integration.dto.PurchaseOrderResource;
import demo.integration.dto.assemblers.LineItemAssembler;
import demo.models.LineItem;
import demo.services.PlantNotAvailableException;
import demo.services.rentit.RestRentalService;
import demo.services.rentit.team4.dto.T4CustomerResource;
import demo.services.rentit.team4.dto.T4PlantResource;
import demo.services.rentit.team4.dto.T4PurchaseOrderResource;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by ostap_000 on 16/12/2014.
 */

@Service
public class Team4RentalService extends RestRentalService {

    @Autowired
    SimpleDateFormat formatter;

    LineItemAssembler lineItemAssembler = new LineItemAssembler();

    @Override
    public List<PlantResource> findAvailablePlants(String plantName, Date startDate, Date endDate) {
        T4PlantResource[] plants = restTemplate.getForObject(
                getRestUrl() + "/plants?name={name}&startDate={start}&endDate={end}",
                T4PlantResource[].class,  plantName, formatter.format(startDate), formatter.format(endDate));

        System.out.println("SIze " + plants.length);
        List<PlantResource> plantResources = new ArrayList<>();
        for (T4PlantResource plant : plants) {
            plantResources.add(toOurPlantResource(plant));
        }
        return plantResources;
    }

    @Override
    public List<PlantResource> getPlants() {
        PlantResource[] plants = restTemplate.getForObject(
                getRestUrl() + "/plants",
                PlantResource[].class);
        return Arrays.asList(plants);
    }

    @Override
    public PurchaseOrderResource createPurchaseOrder(LineItem lineItem) throws RestClientException, PlantNotAvailableException {
        T4PurchaseOrderResource po = new T4PurchaseOrderResource();

        T4PlantResource plant = getT4Plant(lineItem.getPlantRef());
        po.setPlant(plant);
        po.setStartDate(lineItem.getStartDate());
        po.setEndDate(lineItem.getEndDate());

        T4CustomerResource customer = new T4CustomerResource();
        customer.setName("team2");
        customer.setVatNumber("VAT235232");
        po.setCustomer(customer);

        StringWriter sw = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(sw,po);
            System.out.println(sw.getBuffer().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        ResponseEntity<T4PurchaseOrderResource> result = restTemplate.postForEntity(getRestUrl() + "/pos", po, T4PurchaseOrderResource.class);

        if (result.getStatusCode().equals(HttpStatus.CONFLICT))
            throw new PlantNotAvailableException();

        po = result.getBody();

        PurchaseOrderResource purchaseOrderResource = toOurPOResource(po);
        return purchaseOrderResource;
    }


    @Override
    public PurchaseOrderResource getPurchaseOrder(String link) {
        try{
            return toOurPOResource(get(link, T4PurchaseOrderResource.class));
    } catch (HttpStatusCodeException ex) {
        int statusCode = ex.getStatusCode().value();
            ex.printStackTrace();
            System.out.println("STATUS CODE " + statusCode);
    } catch (RestClientException ex) {
            ex.printStackTrace();
    }
        return null;
    }

    private T4PlantResource getT4Plant(String link) {
        return get(link, T4PlantResource.class);
    }

    @Override
    public PlantResource getPlant(String link) {
        return toOurPlantResource(getT4Plant(link));
    }

    private <T> T get(String link, Class<T> clazz){
        ResponseEntity<T> result = restTemplate.getForEntity(link, clazz);
        return result.getBody();
    }



    @Override
    public PlantResource getPlant(Long id) {
        return restTemplate.getForObject(
                getRestUrl() + "/plants/" + id,
                PlantResource.class);
    }

    @Override
    public void rejectPurchaseOrder(Long id) {
        restTemplate.delete(getRestUrl() + "/pos/" + id + "/accept");
    }

    @Override
    public PurchaseOrderResource updatePurchaseOrder(Long id, PurchaseOrderResource purchaseOrder) throws PlantNotAvailableException {
        ResponseEntity<T4PurchaseOrderResource> result = restTemplate.postForEntity(getRestUrl() + "/pos/" + id + "/updates", purchaseOrder.getEndDate(),T4PurchaseOrderResource.class);
        if (result.getStatusCode().equals(HttpStatus.CONFLICT))
            throw new PlantNotAvailableException();
        return toOurPOResource(result.getBody());
    }

    @Override
    public void updatePurchaseOrder(PurchaseOrderResource purchaseOrder) throws RestClientException, PlantNotAvailableException {
        restTemplate.put(getRestUrl() + "/pos", purchaseOrder);
    }

    @Override
    public void cancelPO(PurchaseOrderResource po) {
        restTemplate.delete(getRestUrl() + "/pos/" + po.getIdRes());
    }

    @Override
    public String getStatus(Long id) {
        return restTemplate.getForEntity(getRestUrl() + "/pos/" + id, T4PurchaseOrderResource.class).getBody().getStatus();
    }

    private PlantResource toOurPlantResource(T4PlantResource plant){
        PlantResource plantResource = new PlantResource();
        plantResource.setIdRes(plant.getPlantId());
        plantResource.setName(plant.getName());
        plantResource.setDescription(plant.getDescription());
        plantResource.setPrice(plant.getPrice());
        plantResource.add(plant.getId());
        return plantResource;
    }

    private PurchaseOrderResource toOurPOResource(T4PurchaseOrderResource po){
        PurchaseOrderResource purchaseOrderResource = new PurchaseOrderResource();
        purchaseOrderResource.setIdRes(po.getPoId());
        purchaseOrderResource.setPlant(toOurPlantResource(po.getPlant()));
        purchaseOrderResource.setStartDate(po.getStartDate());
        purchaseOrderResource.setEndDate(po.getEndDate());
        purchaseOrderResource.add(po.getLinks());
        return purchaseOrderResource;
    }
}

