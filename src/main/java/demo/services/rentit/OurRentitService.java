package demo.services.rentit;

import demo.integration.dto.CustomerResource;
import demo.integration.dto.assemblers.LineItemAssembler;
import demo.integration.dto.LineItemResource;
import demo.integration.dto.PlantResource;
import demo.integration.dto.PurchaseOrderResource;
import demo.models.LineItem;
import demo.services.PlantNotAvailableException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by ostap_000 on 02/12/2014.
 */

@Service
public class OurRentitService extends RestRentalService {

    @Autowired
    SimpleDateFormat formatter;

    LineItemAssembler lineItemAssembler = new LineItemAssembler();

    @Override
    public List<PlantResource> findAvailablePlants(String plantName, Date startDate, Date endDate) {
        PlantResource[] plants = restTemplate.getForObject(
        		getRestUrl() + "/plants?name={name}&startDate={start}&endDate={end}",
                PlantResource[].class,  plantName, formatter.format(startDate), formatter.format(endDate));

        return Arrays.asList(plants);
    }

    @Override
    public List<PlantResource> getPlants() {
        PlantResource[] plants = restTemplate.getForObject(
                getRestUrl() + "/plants",
                PlantResource[].class);
        return Arrays.asList(plants);
    }

    @Override
    public PurchaseOrderResource createPurchaseOrder(LineItem lineItem) throws RestClientException, PlantNotAvailableException {
        PurchaseOrderResource po = new PurchaseOrderResource();

        PlantResource plant = getPlant(lineItem.getPlantRef());
        po.setPlant(plant);
        po.setStartDate(lineItem.getStartDate());
        po.setEndDate(lineItem.getEndDate());

        ResponseEntity<PurchaseOrderResource> result = restTemplate.postForEntity(getRestUrl() + "/pos", po, PurchaseOrderResource.class);

        if (result.getStatusCode().equals(HttpStatus.CONFLICT))
            throw new PlantNotAvailableException();

        return result.getBody();
    }


    @Override
    public PurchaseOrderResource getPurchaseOrder(String link) {
        return get(link, PurchaseOrderResource.class);
    }

    @Override
    public PlantResource getPlant(String link) {
        return get(link, PlantResource.class);
    }

    private <T> T get(String link, Class<T> clazz){
        ResponseEntity<T> result = restTemplate.getForEntity(link, clazz);
        return result.getBody();
    }



    @Override
    public PlantResource getPlant(Long id) {
        return restTemplate.getForObject(
                getRestUrl() + "/plants/" + id,
                PlantResource.class);
    }

    @Override
    public void rejectPurchaseOrder(Long id) {
        restTemplate.delete(getRestUrl() + "/pos/" + id + "/accept");
    }

    @Override
    public PurchaseOrderResource updatePurchaseOrder(Long id, PurchaseOrderResource purchaseOrder) throws PlantNotAvailableException {

        StringWriter sw = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(sw,purchaseOrder);
            System.out.println(sw.getBuffer().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        ResponseEntity<PurchaseOrderResource> result = restTemplate.postForEntity(getRestUrl() + "/pos/" + id + "/updates", purchaseOrder,PurchaseOrderResource.class);


        if (result.getStatusCode().equals(HttpStatus.CONFLICT))
            throw new PlantNotAvailableException();
        return result.getBody();
    }

    @Override
    public void updatePurchaseOrder(PurchaseOrderResource purchaseOrder) throws RestClientException, PlantNotAvailableException {
        restTemplate.put(getRestUrl() + "/pos", purchaseOrder);
    }

    @Override
    public void cancelPO(PurchaseOrderResource po) {
        restTemplate.delete(getRestUrl() + "/pos/" + po.getIdRes());
    }

    @Override
    public String getStatus(Long id) {
        return restTemplate.getForEntity(getRestUrl() + "/pos/" + id + "/status", String.class).getBody();
    }
}
