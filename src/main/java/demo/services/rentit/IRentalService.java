package demo.services.rentit;

import demo.integration.dto.PlantResource;
import demo.integration.dto.PurchaseOrderResource;
import demo.models.LineItem;
import demo.models.Supplier;
import demo.services.PlantNotAvailableException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import java.util.Date;
import java.util.List;

/**
 * Created by ostap_000 on 02/12/2014.
 */
@Service
public interface IRentalService {

    public void init(Supplier supplier);

    public List<PlantResource> findAvailablePlants(String plantName, Date startDate, Date endDate);

    public List<PlantResource> getPlants();

    public PurchaseOrderResource createPurchaseOrder(LineItem lineItem) throws RestClientException, PlantNotAvailableException;

    public PurchaseOrderResource getPurchaseOrder(String link);

    public PlantResource getPlant(String link);

    public PlantResource getPlant(Long id);

    public void rejectPurchaseOrder(Long id);

    public PurchaseOrderResource updatePurchaseOrder(Long id,PurchaseOrderResource purchaseOrder) throws PlantNotAvailableException;

    public void updatePurchaseOrder(PurchaseOrderResource purchaseOrder) throws RestClientException, PlantNotAvailableException;

    public void cancelPO(PurchaseOrderResource po);

    public String getStatus(Long id);
}
