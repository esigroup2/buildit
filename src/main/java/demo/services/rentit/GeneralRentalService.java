package demo.services.rentit;

import demo.models.Supplier;

/**
 * Created by ostap_000 on 02/12/2014.
 */
public abstract class GeneralRentalService implements IRentalService {

    protected String host;

    protected String port;

    @Override
    public void init(Supplier supplier) {
        this.host = supplier.getHost();
        this.port = supplier.getPort();
    }

    public String getRestUrl(){
        System.out.println("HOST : " + host);
        return "http://" + host +":" + port + "/rest";
    }
}
