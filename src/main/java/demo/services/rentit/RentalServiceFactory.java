package demo.services.rentit;

import demo.models.Supplier;
import demo.services.rentit.team3.Team3RentalService;
import demo.services.rentit.team4.Team4RentalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by ostap_000 on 02/12/2014.
 */
@Service
public class RentalServiceFactory {


    @Autowired
    OurRentitService ourRentitService;

    @Autowired
    Team3RentalService team3RentitService;

    @Autowired
    Team4RentalService team4RentitService;

    public IRentalService get(Supplier supplier){
        switch (supplier.getName()){
            case "our_rentit":
                ourRentitService.init(supplier);
                return ourRentitService;
            case "team3":
                team3RentitService.init(supplier);
                return team3RentitService;
            case "team4":
                team4RentitService.init(supplier);
                return team4RentitService;
            default:
            break;
        }
        return null;
    }

}
