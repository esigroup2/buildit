package demo.services.rentit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by ostap_000 on 02/12/2014.
 */

@Service
public abstract class RestRentalService extends GeneralRentalService {

    @Autowired
    protected RestTemplate restTemplate;

}
