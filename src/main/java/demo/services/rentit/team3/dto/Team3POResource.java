package demo.services.rentit.team3.dto;

import demo.integration.dto.CustomerResource;
import demo.integration.dto.PlantResource;
import demo.util.ResourceSupport;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by ostap_000 on 16/12/2014.
 */
public class Team3POResource extends ResourceSupport {


    Long idRes;
    PlantResource plant;
    CustomerResource customer;

    @DateTimeFormat(iso= DateTimeFormat.ISO.DATE)
    Date startDate;

    @DateTimeFormat(iso= DateTimeFormat.ISO.DATE)
    Date endDate;
    Float cost;
}