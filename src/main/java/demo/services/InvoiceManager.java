package demo.services;

import java.io.StringReader;
import java.util.List;

import javax.mail.MessagingException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import demo.integration.dto.SupplierResource;
import demo.models.Supplier;
import demo.models.repositories.SupplierRepository;
import demo.services.mail.T3Invoice;
import demo.services.mail.T4Invoice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;

import demo.integration.dto.InvoiceResource;
import demo.models.Invoice;
import demo.models.InvoiceStatus;
import demo.models.repositories.InvoiceRepository;

@Service
public class InvoiceManager {
	
	@Autowired
    @Qualifier("invoiceRepository")
    InvoiceRepository invoiceRepository;

	@Autowired
	SupplierRepository supplierRepo;
	
	@Autowired
	RemittanceAdviceManager ram;
	
	private Invoice parseXml(String xml) throws Exception{

		List<Supplier> suppliers = supplierRepo.findAll();
		Supplier supplier = null;
		for (Supplier possibleSupplier : suppliers) {
			if (xml.indexOf(possibleSupplier.getHost()) != -1) {
				supplier = possibleSupplier;
				break;
			}
		}
		return parseInvoice(supplier,xml);
	}
	
    public void processInvoice(String xml) throws Exception{
//        SAXParserFactory factory = SAXParserFactory.newInstance(); 
//        SAXParser parser = factory.newSAXParser(); 
//        SAXHandler saxp = new SAXHandler(); 
//        System.out.println("Normal Invoice processing...");
//        parser.parse(new InputSource(new StringReader(xml)), saxp);
//        InvoiceResource invoice = saxp.getResult();
//        invoice.setStatus(InvoiceStatus.PENDING);
//        System.out.println(invoice.getDueDate());

		Invoice invoice = parseXml(xml);
        invoiceRepository.saveAndFlush(invoice);
    }
    

    public Invoice processInvoiceFast(String xml) throws Exception{
      Invoice invoice = parseXml(xml);
      invoice.setStatus(InvoiceStatus.ACCEPTED);
      invoice = invoiceRepository.saveAndFlush(invoice);
      return invoice;
  }
    
    public List<Invoice> getUnpaidInvoices(){
    	List<Invoice> res = invoiceRepository.findUnpaid();
    	return res;
    }
    
    public List<Invoice> getInvoices(){
    	List<Invoice> res = invoiceRepository.findAll();
    	return res;
    }
    
    public Invoice acceptInvoice (Long id){
    	Invoice inv = invoiceRepository.findOne(id);
    	inv.setStatus(InvoiceStatus.ACCEPTED);
    	invoiceRepository.saveAndFlush(inv);
    	try {
			ram.sendRemmitance(inv);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
    	return inv;
    }
    
    public Invoice rejectInvoice (Long id){
    	Invoice inv = invoiceRepository.findOne(id);
    	inv.setStatus(InvoiceStatus.REJECTED);
    	invoiceRepository.saveAndFlush(inv);
    	return inv;
    }


	public Invoice parseInvoice(Supplier supplier, String xml) throws JAXBException {
		if (supplier.getId() == 1){
			JAXBContext jaxbContext = JAXBContext.newInstance(InvoiceResource.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			StringReader reader = new StringReader(xml);
			InvoiceResource invoiceRes = (InvoiceResource) unmarshaller.unmarshal(reader);
			Invoice invoice = new Invoice();
			invoice.setDueDate(invoiceRes.getDueDate());
			invoice.setStatus(InvoiceStatus.PENDING);
			invoice.setPurchaseOrderRef(invoiceRes.getPurchaseOrder().getId().getHref());
			invoice.setTotal(invoiceRes.getTotal());
			return invoice;
		}else if (supplier.getId() == 2){
			JAXBContext jaxbContext = JAXBContext.newInstance(T3Invoice.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			StringReader reader = new StringReader(xml);
			T3Invoice invoiceRes = (T3Invoice) unmarshaller.unmarshal(reader);
			Invoice invoice = new Invoice();
			invoice.setDueDate(invoiceRes.getDate());
			invoice.setStatus(InvoiceStatus.PENDING);
			invoice.setPurchaseOrderRef(invoiceRes.getPurchaseOrderRef());
			invoice.setTotal(invoiceRes.getTotal());
			invoice.setProviderId(Long.toString(invoiceRes.getIdRes()));
			return invoice;
		}else if (supplier.getId() == 3){
			JAXBContext jaxbContext = JAXBContext.newInstance(T4Invoice.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			StringReader reader = new StringReader(xml);
			T4Invoice invoiceRes = (T4Invoice) unmarshaller.unmarshal(reader);
			Invoice invoice = new Invoice();
			invoice.setDueDate(invoiceRes.getDueDate());
			invoice.setStatus(InvoiceStatus.PENDING);
			invoice.setPurchaseOrderRef(invoiceRes.getPurchaseOrderRef());
			invoice.setTotal(invoiceRes.getTotalCost());
			return invoice;
		}
		return null;
	}
}
