package demo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import demo.models.LineItem;
import demo.models.LineItemStatus;
import demo.models.PHRStatus;
import demo.models.PlantHireRequest;
import demo.models.repositories.LineItemRepository;
import demo.models.repositories.PlantHireRequestRepository;
import demo.services.rentit.RentalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * Created by ostap_000 on 13/12/2014.
 */

@Configuration
public class ScheduledTasks {

    @Autowired
    LineItemRepository repo;

    @Autowired
    RentalService rentalService;

    @Scheduled(fixedRate = 60_000)
    public void updateStatuses() {
        List<LineItem> items = repo.findByStatus(LineItemStatus.APPROVED);
        for (LineItem item : items) {
            String status = rentalService.getStatus(item);
            if (status.equals("REJECTED")){
                System.out.println("# " + item.getId() +" Item status changed from " + item.getStatus() + " to REJECTED_BY_SUPPLIER");
                item.setStatus(LineItemStatus.REJECTED_BY_SUPPLIER);
            }
            if (status.equals("CLOSED")){
                System.out.println("# " + item.getId() +" Item status changed from " + item.getStatus() + " to REJECTED_BY_SUPPLIER");
                item.setStatus(LineItemStatus.CLOSED);
            }
            repo.saveAndFlush(item);
        }
    }
}