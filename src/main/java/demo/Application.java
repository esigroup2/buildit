package demo;

import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.WebMvcProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import demo.models.SiteEngineer;

import org.springframework.http.converter.HttpMessageConverter;

@Configuration
@ComponentScan
@EnableAutoConfiguration
@ImportResource({ "classpath:META-INF/integration/integration-mail.xml",
		"classpath:META-INF/integration/integration.xml" })
@EnableScheduling
public class Application {

	@Autowired
	private WebMvcProperties mvcProperties = new WebMvcProperties();

	public class BasicSecureSimpleClientHttpRequestFactory extends
			SimpleClientHttpRequestFactory {
		@Autowired
		Credentials credentials;

		public BasicSecureSimpleClientHttpRequestFactory() {
		}

		@Override
		public ClientHttpRequest createRequest(URI uri, HttpMethod httpMethod)
				throws IOException {
			ClientHttpRequest result = super.createRequest(uri, httpMethod);
			System.out.println(uri);
			System.out.println(uri.getAuthority());
			System.out.println(credentials.getCredentials());

			for (Map<String, String> map : credentials.getCredentials()
					.values()) {
				String authority = map.get("authority");
				if (authority != null && authority.equals(uri.getAuthority())) {
					result.getHeaders().add("Authorization",
							map.get("authorization"));
					break;
				}
			}

			if (credentials.getCredentials().containsKey(uri.getAuthority())) {
			}
			return result;
		}
	}
	
	@Bean
	public ClientHttpRequestFactory requestFactory() {
		return new BasicSecureSimpleClientHttpRequestFactory();
	}
	
	@Autowired
	ClientHttpRequestFactory interceptor;

	@Bean
	public RestTemplate restTemplate() {
		RestTemplate _restTemplate = new RestTemplate(interceptor);
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(new MappingJackson2HttpMessageConverter());
		_restTemplate.setMessageConverters(messageConverters);
		_restTemplate.setErrorHandler(new CustomResponseErrorHandler());
		return _restTemplate;
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public SimpleDateFormat dateFormatter() {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormatter;
	}


	@Bean
	@ConfigurationProperties(locations = "classpath:META-INF/integration/credentials.yml")
	public Credentials getCredentials() {
		return new Credentials();
	}

	public static class Credentials {
		private Map<String, Map<String, String>> credentials = new HashMap<>();

		public Map<String, Map<String, String>> getCredentials() {
			return this.credentials;
		}
	}

}
