package demo.util;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ostap_000 on 01/12/2014.
 */
public class Utils {

    public static LocalDate toLocalDate(Date date){
        if (date instanceof java.sql.Date){
            return ((java.sql.Date) date).toLocalDate();
        }else{
            return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        }
    }

    public static int getPeriod(Date from, Date to){
        return Period.between(toLocalDate(from), toLocalDate(to)).getDays() + 1;
    }

    public static Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

}
