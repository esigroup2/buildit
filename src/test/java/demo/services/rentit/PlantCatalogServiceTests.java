package demo.services.rentit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import demo.Application;
import demo.CustomResponseErrorHandler;
import demo.SimpleDbConfig;
import demo.integration.dto.PlantResource;
import demo.integration.dto.UpdatePHRResource;
import demo.models.PHRStatus;
import demo.models.PlantHireRequest;
import demo.models.UpdatePHR;
import demo.models.repositories.PlantHireRequestRepository;
import demo.services.PlantHireRequestManager;
import demo.services.PlantNotAvailableException;
import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { Application.class,
		SimpleDbConfig.class })
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class PlantCatalogServiceTests {

	@Configuration
	static class TestConfiguration {
		@Bean
		public RestTemplate restTemplate() {
			RestTemplate _restTemplate = new RestTemplate();
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			_restTemplate.setMessageConverters(messageConverters);
            _restTemplate.setErrorHandler(new CustomResponseErrorHandler());
			return _restTemplate;
		}

		@Bean
		public RentalService rentalService() {
			return new RentalService();
		}

		@Bean
		public PlantHireRequestManager phrManager() {
			return new PlantHireRequestManager();
		}

	}

	@Autowired
	private RentalService rentItProxy;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private PlantHireRequestRepository phrRepository;

	@InjectMocks
	@Autowired
	private PlantHireRequestManager phrManager;

	private ObjectMapper mapper;


    List<ClientHttpRequestInterceptor> interceptors;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mapper = new ObjectMapper();
        interceptors = restTemplate.getInterceptors();
    }

    @After
    public void tearDown() {
        restTemplate.setInterceptors(interceptors);
    }

	// @Test
	// public void testFindAvailablePlants() throws Exception {
	// Resource responseBody = new ClassPathResource("AvailablePlantsV1.json",
	// this.getClass());
	// List<PlantResource> list =
	// mapper.readValue(responseBody.getFile(),mapper.getTypeFactory().constructCollectionType(List.class,
	// PlantResource.class));
	// List<PlantResource> result = rentItProxy.findAvailablePlants("Excavator",
	// new LocalDate(2014, 10, 6).toDate(), new LocalDate(2014, 10,
	// 10).toDate());
	//
	// mockServer.verify();
	// assertEquals(result, list);
	// }
//
//	@Test(expected=PlantNotAvailableException.class)
//	public void testCreatePlantHireRequestRejected() throws Exception {
//
//		Date startDate = new LocalDate(2014, 10, 6).toDate();
//		Date endDate = new LocalDate(2014, 10, 10).toDate();
//
//		List<PlantResource> plants = rentItProxy.findAvailablePlants(
//				"Excavator", startDate, endDate);
//		PlantHireRequest phr = new PlantHireRequest();
//		phr.setPlantRef(plants.get(0).getId().getHref());
//		phr.setStartDate(startDate);
//		phr.setEndDate(endDate);
//
//		phr = phrManager.createPlantHireRequest(phr);
//
//		ClientHttpRequestInterceptor interceptor = (request, body, execution) -> {
//            request.getHeaders().add("prefer", "409");
//            return execution.execute(request, body);
//        };
//		restTemplate.setInterceptors(Collections.singletonList(interceptor));
//		phrManager.approvePlantHireRequest(phr.getId());
//	}
//
//	@Test
//	public void testCreatePlantHireRequest() throws Exception {
//		Date startDate = new LocalDate(2014, 10, 6).toDate();
//		Date endDate = new LocalDate(2014, 10, 10).toDate();
//
//		List<PlantResource> plants = rentItProxy.findAvailablePlants(
//				"Excavator", startDate, endDate);
//		PlantHireRequest phr = new PlantHireRequest();
//		phr.setPlantRef(plants.get(0).getId().getHref());
//		phr.setStartDate(startDate);
//		phr.setEndDate(endDate);
//
//		phr = phrManager.createPlantHireRequest(phr);
//		phr = phrManager.approvePlantHireRequest(phr.getId());
//		assertEquals(phr.getPurchaseOrderRef(),
//				"http://localhost:3000/rest/pos/10001");
//	}
//
//    @Test
//    public void testUpdatePO() throws Exception {
//        Date startDate = new LocalDate(2014, 10, 6).toDate();
//        Date endDate = new LocalDate(2014, 10, 10).toDate();
//
//        Date extStartDate = new LocalDate(2014, 11, 6).toDate();
//        Date extEndDate = new LocalDate(2014, 11, 10).toDate();
//
//        List<PlantResource> plants = rentItProxy.findAvailablePlants(
//                "Excavator", startDate, endDate);
//
//        PlantHireRequest phr = new PlantHireRequest();
//        phr.setPlantRef(plants.get(0).getId().getHref());
//        phr.setStartDate(startDate);
//        phr.setEndDate(endDate);
//
//        phr = phrManager.createPlantHireRequest(phr);
//        phr = phrManager.approvePlantHireRequest(phr.getId());
//
//        UpdatePHRResource updatePHRR = new UpdatePHRResource();
//        updatePHRR.setStartDate(extStartDate);
//        updatePHRR.setEndDate(extEndDate);
//
//        UpdatePHR updatePHR = phrManager.extendPHR(1l, updatePHRR);
//        updatePHR = phrManager.approveUpdatePHR(updatePHR.getId());
//        assertEquals(updatePHR.getStatus(), PHRStatus.APPROVED);
//    }
//
//    @Test(expected=PlantNotAvailableException.class)
//	public void testUpdatePOError() throws Exception {
//        Date startDate = new LocalDate(2014, 10, 6).toDate();
//        Date endDate = new LocalDate(2014, 10, 10).toDate();
//
//        Date extStartDate = new LocalDate(2014, 11, 6).toDate();
//        Date extEndDate = new LocalDate(2014, 11, 10).toDate();
//
//        List<PlantResource> plants = rentItProxy.findAvailablePlants(
//                "Excavator", startDate, endDate);
//
//        PlantHireRequest phr = new PlantHireRequest();
//        phr.setPlantRef(plants.get(0).getId().getHref());
//        phr.setStartDate(startDate);
//        phr.setEndDate(endDate);
//
//        phr = phrManager.createPlantHireRequest(phr);
//        phr = phrManager.approvePlantHireRequest(phr.getId());
//
//        UpdatePHRResource updatePHRR = new UpdatePHRResource();
//        updatePHRR.setStartDate(extStartDate);
//        updatePHRR.setEndDate(extEndDate);
//
//		ClientHttpRequestInterceptor interceptor = (request, body, execution) -> {
//			request.getHeaders().add("prefer", "409");
//			return execution.execute(request, body);
//		};
//		restTemplate.setInterceptors(Collections.singletonList(interceptor));
//
//        UpdatePHR updatePHR = phrManager.extendPHR(1L, updatePHRR);
//	}

}
