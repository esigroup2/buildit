//package demo;
//
//import java.sql.Date;
//import java.sql.PreparedStatement;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.util.List;
//
//import org.dbunit.IDatabaseTester;
//import org.dbunit.JdbcDatabaseTester;
//import org.openqa.selenium.By.ById;
//import org.openqa.selenium.By;
//import org.openqa.selenium.Keys;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
//
//import cucumber.api.DataTable;
//import cucumber.api.PendingException;
//import cucumber.api.java.After;
//import cucumber.api.java.Before;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//import demo.models.Plant;
//import static org.junit.Assert.assertEquals;
//
//public class QueryPlantCatalogSteps {
//
//	IDatabaseTester db = null;
//	
//	WebDriver user = null;
//	    
//    @Before
//    public void beforeScenario() throws Throwable{
//    	if (db == null) {
//	    	db = new JdbcDatabaseTester("org.postgresql.Driver", "jdbc:postgresql://localhost:5432/demo", "postgres", "12345678");
//    	}
//    	
////    	String sql = "insert into site_engineer (id, name, s_security_number) values (?, ?, ?)";
////    	PreparedStatement ps = db.getConnection().getConnection().prepareStatement(sql);
////		ps.setInt(1, 0);
////		ps.setString(2, "defaultSiteEngineer");
////		ps.setString(3, "007");
////		ps.execute();
////		ps.close();
//		
//    	System.setProperty("webdriver.chrome.driver", "C:\\Programs\\chromedriver.exe");
//    	user = new ChromeDriver();
//    }
//    
//    @After
//    public void afterScenario() throws Exception {
//    	Statement st = db.getConnection().getConnection().createStatement();
//    	st.executeUpdate("delete from plant_hire_request");
//    	st.executeUpdate("delete from plant");
//    	st.close();
//    	user.close();
//    }
//	
//	@Given("^the following plants are currently available for rental$")
//	public void the_following_plants_are_currently_available_for_rental(List<Plant> catalog) throws Throwable {
//		String sql = "insert into plant (id, name, description, price) values (?, ?, ?, ?)";
//		PreparedStatement ps = db.getConnection().getConnection().prepareStatement(sql);
//		
//		for (int i = 0; i < catalog.size(); i++) {
//			Plant plant = catalog.get(i);
//			ps.setInt(1, i + 1);
//			ps.setString(2, plant.getName());
//			ps.setString(3, plant.getDescription());
//			ps.setFloat(4, plant.getPrice());
//			ps.addBatch();
//		}
//		
//		ps.executeBatch();
//		ps.close();
//	}
//
//	@Given("^I am in the \"(.*?)\" web page$")
//	public void i_am_in_the_web_page(String arg1) throws Throwable {
//	    user.get("http://localhost:8080/phrs/form");
//	}
//
//	@Given("^No Plant Hire Request exists in the system$")
//	public void no_Plant_Hire_Request_exists_in_the_system() throws Throwable {
//    	Statement st = db.getConnection().getConnection().createStatement();
//    	st.executeUpdate("delete from plant_hire_request");
//    	st.close();
//	}
//
//	@When("^I query the plant catalog for an \"(.*?)\" available from \"(.*?)\" to \"(.*?)\"$")
//	public void i_query_the_plant_catalog_for_an_available_from_to(String name, String startDate, String endDate) throws Throwable {
//		WebElement plantNameEdit = user.findElement(By.id("PlantNameID"));
//		plantNameEdit.click();
//		plantNameEdit.sendKeys(name);
//		
//		WebElement startDateEdit = user.findElement(By.id("StartDateID"));
//		startDateEdit.click();
//		startDateEdit.sendKeys(Keys.ARROW_LEFT);
//		startDateEdit.sendKeys(Keys.ARROW_LEFT);
//		startDateEdit.sendKeys(startDate);
//		
//		WebElement endDateEdit = user.findElement(By.id("EndDateID"));
//		endDateEdit.click();
//		endDateEdit.sendKeys(Keys.ARROW_LEFT);
//		endDateEdit.sendKeys(Keys.ARROW_LEFT);
//		endDateEdit.sendKeys(endDate);
//		
//		WebElement submitButton = user.findElement(By.id("SubmitButtonID"));
//		submitButton.click();
//	}
//
//	@Then("^I should have (\\d+) plants being shown$")
//	public void i_should_have_plants_being_shown(int expectedCount) throws Throwable {
//		int rowCount = user.findElements(By.cssSelector(".PlantRow")).size();
//		assertEquals(expectedCount, rowCount);
//	}
//	
//	@Given("^I am logged in as Site Engineer$")
//	public void i_am_logged_in_as_Site_Engineer() throws Throwable {
//	    // nothing todo yet
//	}
//
//	@When("^I select \"(.*?)\"$")
//	public void i_select(String name) throws Throwable {
//		WebElement button = user.findElement(By.xpath(String.format("//tr[td//text()[contains(., '%s')]]/td[4]//button", name)));
//		button.click();
//	}
//
//	@Then("^a Plant Hire Request should be created with a total price of (\\d+)\\.(\\d+)$")
//	public void a_Plant_Hire_Request_should_be_created_with_a_total_price_of(int arg1, int arg2) throws Throwable {
//		WebElement totalPriceText = user.findElement(By.id("TotalPriceID"));
//	    assertEquals(totalPriceText.getText(),arg1 + "." + arg2);
//	}
//	
//	@Given("^I am logged in as Works Engineer$")
//	public void i_am_logged_in_as_Works_Engineer() throws Throwable {
//	    // nothing todo yet
//	}
//
//	@Given("^there is a plant hire request with status 'pending'$")
//	public void there_is_a_plant_hire_request_with_status_pending() throws Throwable {
//    	String sql = "insert into plant_hire_request (id, end_date, start_date,price,"
//    			+ "plant_id,site_engineer_id,supplier,address,status) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
//    	PreparedStatement ps = db.getConnection().getConnection().prepareStatement(sql);
//		ps.setInt(1, 0);
//		ps.setDate(2,Date.valueOf("2014-09-24"));
//		ps.setDate(3,Date.valueOf("2014-09-22"));
//		ps.setFloat(4, 200.0f);
//		ps.setLong(5,1);
//		ps.setLong(6,0);
//		ps.setString(7, "defaultSupplier");
//		ps.setString(8, "defaultAddress");
//		ps.setString(9,"pending");
//		ps.execute();
//		ps.close();
//		
//		user.get("http://localhost:8080/phrs");
//	}
//
//	@When("^I approve the plant hire request$")
//	public void i_approve_the_plant_hire_request() throws Throwable {
//		user.findElement(By.xpath("//a[contains(@href, '/phrs/0/approve')]")).click();
//	}
//
//	@Then("^the status should be 'accepted'$")
//	public void the_status_should_be_accepted() throws Throwable {
//		WebElement statusText = user.findElement(By.xpath(String.format("//tr[td//text()[contains(., '%s')]]/td[7]", "0")));
//		assertEquals(statusText.getText(),"approved");
//	}
//
//	
//}
