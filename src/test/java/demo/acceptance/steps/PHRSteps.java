package demo.acceptance.steps;

import java.sql.Statement;
import java.sql.PreparedStatement;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.assertEquals;

public class PHRSteps {
	
	IDatabaseTester db = null;
	WebDriver seng = null;
	WebDriver weng = null;
	
    @Before
    public void beforeScenario() throws Throwable {
    	if (db == null) {
	    	db = new JdbcDatabaseTester("org.postgresql.Driver", "jdbc:postgresql://localhost:5432/postgres", "postgres", "12345678");
    	}

    	System.setProperty("webdriver.chrome.driver", "C:\\Programs\\chromedriver.exe");
		weng = new ChromeDriver();
    	seng = new ChromeDriver();
    	seng.get("http://localhost:8080/#/login");
    	seng.findElement(By.xpath("//div[1]/input")).sendKeys("admin");
    	seng.findElement(By.xpath("//div[2]/input")).sendKeys("admin");
    	seng.findElement(By.id("SubmitButtonID")).click();
    	
    	weng.get("http://localhost:8080/#/login");
    	weng.findElement(By.xpath("//div[1]/input")).sendKeys("admin");
    	weng.findElement(By.xpath("//div[2]/input")).sendKeys("admin");
    	weng.findElement(By.id("SubmitButtonID")).click();
    }

    @After
    public void afterScenarion(){
        seng.close();
        weng.close();
    }
    
	@Given("^SE in \"(.*?)\" web page$")
	public void se_in_web_page(String arg1) throws Throwable {
		seng.get("http://localhost:8080/#/phrs/create");
	}
	
	@Given("^No Plant Hire Request exists in the system$")
	public void no_Plant_Hire_Request_exists_in_the_system() throws Throwable {
    	Statement st = db.getConnection().getConnection().createStatement();
    	st.executeUpdate("delete from updatephr");
    	st.executeUpdate("delete from plant_hire_request");
    	st.close();
	}

	@Given("^SE creates PHR with dates for an \"(.*?)\" from \"(.*?)\" to \"(.*?)\" for \"(.*?)\"$")
	public void se_creates_PHR_with_dates_for_an_from_to_for(String plantName, String startDate, String endDate, String description) throws Throwable {
		seng.findElement(By.id("PlantNameID")).sendKeys(plantName);
		seng.findElement(By.id("StartDateID")).sendKeys(startDate);
		seng.findElement(By.id("EndDateID")).sendKeys(endDate);
		seng.findElement(By.id("SubmitButtonID")).click();
		Thread.sleep(1000);
		seng.findElement(By.xpath(String.format("//tr[td//text()[contains(., '%s')]]/td[4]//button", description))).click();
		
	}

	@Given("^WE rejects PHR$")
	public void we_rejects_PHR() throws Throwable {
		weng.get("http://localhost:8080/#/phrs");
		Thread.sleep(1000);
		weng.findElement(By.xpath(String.format("//tr//td[5]//button[contains(.,'reject')]"))).click();
	}
	
	@When("^SE clicks update PHR$")
	public void se_clicks_update_PHR() throws Throwable {
		seng.get("http://localhost:8080/#/phrs");
		Thread.sleep(1000);
		List<WebElement> buttons = seng.findElements(By.xpath(String.format("//button[contains(.,'update')]")));
		buttons.get(buttons.size()-1).click();
	}

	@When("^SE updates PHR with dates for an \"(.*?)\" from \"(.*?)\" to \"(.*?)\" for \"(.*?)\"$")
	public void se_updates_PHR_with_dates_for_an_from_to_for(String plantName, String startDate, String endDate, String description) throws Throwable {
		seng.findElement(By.id("PlantNameID")).sendKeys(plantName);
		seng.findElement(By.id("StartDateID")).sendKeys(startDate);
		seng.findElement(By.id("EndDateID")).sendKeys(endDate);
		seng.findElement(By.id("UpdateButtonID")).click();
		Thread.sleep(1000);
		seng.findElement(By.xpath(String.format("//tr[td//text()[contains(., '%s')]]/td[4]//button", description))).click();
	}

	@Then("^PHR start date should be \"(.*?)\" and End date \"(.*?)\"$")
	public void phr_start_date_should_be_and_End_date(String startDate, String endDate) throws Throwable {
		Thread.sleep(1000);
		String date1[] = seng.findElement(By.xpath("//tr[1]//td[2]")).getText().split("-");
	    String date2[] = seng.findElement(By.xpath("//tr[1]//td[3]")).getText().split("-");
	    assertEquals(startDate, date1[2]+"-"+date1[1]+"-"+date1[0]);
	    assertEquals(endDate, date2[2]+"-"+date2[1]+"-"+date2[0]);
	}
	
	@Given("^SE create PHR with dates from \"(.*?)\" to \"(.*?)\" for \"(.*?)\"$")
	public void i_create_PHR_with_dates_from_to(String startDate, String endDate, String description) throws Throwable {
		Thread.sleep(1000);
		WebElement startDateEdit = seng.findElement(By.id("StartDateID"));
		startDateEdit.sendKeys(startDate);
		
		WebElement endDateEdit = seng.findElement(By.id("EndDateID"));
		endDateEdit.sendKeys(endDate);
		
		WebElement submitButton = seng.findElement(By.id("SubmitButtonID"));
		submitButton.click();

		Thread.sleep(1000);
		WebElement button = seng.findElement(By.xpath(String.format("//tr[td//text()[contains(., '%s')]]/td[4]//button", description)));
		button.click();
	}

	@Given("^WE approved PHR$")
	public void work_engineer_approved_PHR() throws Throwable {
        weng.get("http://localhost:8080/#/phrs");
        Thread.sleep(1000);
        weng.findElement(By.xpath(String.format("//tr//td[5]//button[contains(.,'approve')]"))).click();
	}

	@When("^SE click extend PHR$")
	public void i_click_extend_PHR() throws Throwable {
		seng.get("http://localhost:8080/#/phrs");
		Thread.sleep(1000);
		List<WebElement> buttons = seng.findElements(By.xpath(String.format("//button[contains(.,'extend')]")));
		buttons.get(buttons.size()-1).click();
	}

	@Then("^PHR dates Start date should be \"(.*?)\" End date should be \"(.*?)\"$")
	public void phr_dates_Start_date_should_be_End_date_should_be(String startDate, String endDate) throws Throwable {
		Thread.sleep(1000);
		String date1[] = seng.findElement(By.xpath("//tr[1]//td[2]")).getText().split("-");
	    String date2[] = seng.findElement(By.xpath("//tr[1]//td[3]")).getText().split("-");
	    assertEquals(startDate, date1[2]+"-"+date1[1]+"-"+date1[0]);
	    assertEquals(endDate, date2[2]+"-"+date2[1]+"-"+date2[0]);
	}
	
	@When("^SE change start date to \"(.*?)\" and end date to \"(.*?)\"$")
	public void se_change_end_date_to(String startDate, String endDate) throws Throwable {
		Thread.sleep(1000);

		WebElement startDateEdit = seng.findElement(By.id("StartDateID"));
		startDateEdit.sendKeys(startDate);
		
		WebElement endDateEdit = seng.findElement(By.id("EndDateID"));
		endDateEdit.sendKeys(endDate);
		
		WebElement submitButton = seng.findElement(By.id("ExtendButtonID"));
		submitButton.click();
	}


    @Then("^SE should see error \"([^\"]*)\"$")
    public void SE_should_see_error(String error) throws Throwable {
        Thread.sleep(1000);
        assertEquals(error, seng.findElement(By.xpath("//div[@id='error']")).getText());
    }

    @When("^SE clicks on cancel button$")
    public void SE_clicks_on_cancel_button() throws Throwable {
        Thread.sleep(1000);
        List<WebElement> buttons = seng.findElements(By.xpath(String.format("//button[contains(.,'cancel')]")));
        buttons.get(buttons.size()-1).click();
    }

    @Given("^SE creates PHR with dates for an \"([^\"]*)\" in \"([^\"]*)\" days after now for \"([^\"]*)\"$")
    public void SE_creates_PHR_with_dates_for_an_in_days_after_now_for(String plantName, String period, String description) throws Throwable {
        LocalDate currentDate = LocalDate.now().plusDays(Long.parseLong(period));
        String date = currentDate.getDayOfMonth() + "-" + currentDate.getMonthValue() + "-" + currentDate.getYear();
        
        seng.findElement(By.id("PlantNameID")).sendKeys(plantName);
        seng.findElement(By.id("StartDateID")).sendKeys(date);
        seng.findElement(By.id("EndDateID")).sendKeys(date);
        seng.findElement(By.id("SubmitButtonID")).click();

        Thread.sleep(1000);
        WebElement button = seng.findElement(By.xpath(String.format("//tr[td//text()[contains(., '%s')]]/td[4]//button", description)));
        button.click();
    }

    @Then("^SE should see empty list of PHRs$")
    public void SE_should_see_empty_list_of_PHRs() throws Throwable {
		Thread.sleep(1000);
        List<WebElement> buttons = seng.findElements(By.xpath(String.format("//button[contains(.,'approve')]")));
        assertEquals(0,buttons.size());
    }
}