Feature: Extending Plant Hire Request
	As a site engineer
	I want to create then update rejected PHR
	So I start working with system
	
	Background: Plant catalog
		Given No Plant Hire Request exists in the system
		And SE in "Create Plant Hire Request" web page
		
	Scenario:  Plant hire request successfuly updated
		Given SE creates PHR with dates for an "Excavator" from "29-10-2014" to "30-10-2014" for "3 Tonne Mini excavator"
		And WE rejects PHR
		When SE clicks update PHR
		And SE updates PHR with dates for an "Excavator" from "01-11-2014" to "03-112014" for "15 Tonne Large excavator"
		Then PHR start date should be "01-11-2014" and End date "03-11-2014"
		
	Scenario:  Plant hire request has not been updated
		Given SE creates PHR with dates for an "Excavator" from "29-10-2014" to "30-10-2014" for "3 Tonne Mini excavator"
		And WE rejects PHR
		When SE clicks update PHR
		And SE updates PHR with dates for an "Excavator" from "01-11-2014" to "30-10-2014" for "15 Tonne Large excavator"
		Then SE should see error "New dates are incorrect."