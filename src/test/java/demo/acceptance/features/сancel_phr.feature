Feature: Plant Hire Request Cancellation
  As a site engineer
  I want to cancel a Plant Hire Request
  So that the Plant will not be delivered

 Background: No Plant Hire Requests
    Given No Plant Hire Request exists in the system
    And SE in "Create Plant Hire Request" web page
	    	
	Scenario: Site engineer cancels Plant Hire Request successfully
        Given SE creates PHR with dates for an "Excavator" in "2" days after now for "3 Tonne Mini excavator"
        When SE clicks on cancel button
        Then SE should see empty list of PHRs
	
	Scenario: Supplier cancels PHR
        Given SE creates PHR with dates for an "Excavator" in "0" days after now for "3 Tonne Mini excavator"
        When SE clicks on cancel button
        Then SE should see error "You can't cancel plant hire request now."
  