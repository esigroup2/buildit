Feature: Extending Plant Hire Request
	As a site engineer
	I want to create then extend PHR
	So I start working with system
	
	Background: Plant catalog
		Given No Plant Hire Request exists in the system
		And SE in "Create Plant Hire Request" web page
		
	Scenario:  Plant hire request successfuly extended
		Given SE create PHR with dates from "29-10-2014" to "30-10-2014" for "3 Tonne Mini excavator"
		And WE approved PHR
		When SE click extend PHR
		And SE change start date to "29-10-2014" and end date to "31-10-2014"
		Then PHR dates Start date should be "29-10-2014" End date should be "31-10-2014"

	Scenario: Plant hire request has not been extended
		Given SE create PHR with dates from "29-10-2014" to "30-10-2014" for "3 Tonne Mini excavator"
		And WE approved PHR
		When SE click extend PHR
        And SE change start date to "31-10-2014" and end date to "29-10-2014"
        Then SE should see error "Extension dates are incorrect."
