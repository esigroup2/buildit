package demo.integration.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import demo.Application;
import demo.SimpleDbConfig;
import demo.Application.BasicSecureSimpleClientHttpRequestFactory;
import demo.Application.Credentials;
import demo.integration.dto.PlantHireRequestResource;
import demo.integration.dto.PlantResource;
import demo.integration.dto.PurchaseOrderResource;

import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.net.URI;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = { Application.class,
		SimpleDbConfig.class })
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class PHRRestControllerTest {

	@Configuration
	static class TestConfiguration {
		@Bean
		public RestTemplate restTemplate() {
			RestTemplate _restTemplate = new RestTemplate(new BasicSecureSimpleClientHttpRequestFactory());
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			_restTemplate.setMessageConverters(messageConverters);
			return _restTemplate;
		}

		public class BasicSecureSimpleClientHttpRequestFactory extends
				SimpleClientHttpRequestFactory {

			public BasicSecureSimpleClientHttpRequestFactory() {
			}

			@Override
			public ClientHttpRequest createRequest(URI uri,
					HttpMethod httpMethod) throws IOException {
				ClientHttpRequest result = super.createRequest(uri, httpMethod);

				result.getHeaders().add("Authorization",
						"Basic YWRtaW46YWRtaW4=");
				return result;
			}
		}

	}

	@Autowired
	private WebApplicationContext wac;

	@Autowired
	private RestTemplate restTemplate;

	private MockMvc mockMvc;

	IDatabaseTester db = null;

	@Before
	public void setup() throws SQLException, Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	@DatabaseSetup(value = "EmptyDatabase.xml")
	public void testCreatePlantHireRequest() throws Exception {
		// ObjectMapper mapper = new ObjectMapper();
		// PlantResource plant = new PlantResource();
		// plant.add(new Link("http://localhost:3000/rest/plants/10001"));
		// plant.setPrice(300f);
		//
		// PlantHireRequestResource phr = new PlantHireRequestResource();
		// phr.setPlant(plant);
		// phr.setStartDate(new LocalDate(2014, 10, 6).toDate());
		// phr.setEndDate(new LocalDate(2014, 10, 10).toDate());
		//
		// MvcResult result = mockMvc.perform(post("/rest/phrs")
		// .contentType(MediaType.APPLICATION_JSON)
		// .content(mapper.writeValueAsString(phr))
		// )
		// .andExpect(status().isCreated())
		// // Verify the cost associated with the PHR
		// .andReturn();
		//
		// PlantHireRequestResource phrp =
		// mapper.readValue(result.getResponse().getContentAsString(),
		// PlantHireRequestResource.class);
		// assertThat(phrp.getLink("self"), is(notNullValue()));
		// assertThat(phrp.get_link("approve"), is(notNullValue()));
		// assertThat(phrp.get_link("reject"), is(notNullValue()));
		// assertThat(phrp.getLinks().size(), is(1));
		// assertThat(phrp.get_links().size(), is(2));
	}

	@Test
	@DatabaseSetup(value = "DatabaseWithPendingPHR.xml")
	public void testApprovePlantHireRequest() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));

		Date startDate = new LocalDate(2014, 9, 22).toDate();
		Date endDate = new LocalDate(2014, 9, 24).toDate();

		PlantResource plant = new PlantResource();
		plant.add(new Link("http://localhost:3000/rest/plants/10002"));

		PurchaseOrderResource porRequest = new PurchaseOrderResource();
		porRequest.setStartDate(startDate);
		porRequest.setEndDate(endDate);
		porRequest.setPlant(plant);

		PurchaseOrderResource porResponse = new PurchaseOrderResource();
		porResponse.add(new Link("http://localhost:3000/rest/pos/345"));
		porResponse.setStartDate(startDate);
		porResponse.setEndDate(endDate);
		porResponse.setPlant(plant);

		MvcResult result = mockMvc.perform(get("/rest/phrs/{id}", 10001L))
				.andDo(print())
				.andExpect(status().isOk()).andReturn();
		PlantHireRequestResource phrp = mapper.readValue(result.getResponse()
				.getContentAsString(), PlantHireRequestResource.class);
		assertThat(phrp.get_link("approve"), is(notNullValue()));

		Link approve = phrp.get_link("approve");

		result = mockMvc.perform(post(approve.getHref()))
				.andExpect(status().isAccepted()).andReturn();

		phrp = mapper.readValue(result.getResponse().getContentAsString(),
				PlantHireRequestResource.class);
		assertThat(phrp.getLink("self"), is(notNullValue()));
		assertThat(phrp.get_link("extend"), is(notNullValue()));
		assertThat(phrp.get_link("cancel"), is(notNullValue()));
		assertThat(phrp.getLinks().size() + phrp.get_links().size(), is(3));
	}

	@Test
	@DatabaseSetup(value = "DatabaseWithPendingPHR.xml")
	public void testRejectPlantHireRequest() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));

		MvcResult result = mockMvc.perform(get("/rest/phrs/{id}", 10001L))
				.andExpect(status().isOk()).andReturn();
		PlantHireRequestResource phrp = mapper.readValue(result.getResponse()
				.getContentAsString(), PlantHireRequestResource.class);

		assertThat(phrp.get_link("reject"), is(notNullValue()));

		Link reject = phrp.get_link("reject");

		result = mockMvc.perform(delete(reject.getHref()))
				.andExpect(status().isOk()).andReturn();

		phrp = mapper.readValue(result.getResponse().getContentAsString(),
				PlantHireRequestResource.class);
		assertThat(phrp.getLink("self"), is(notNullValue()));
		assertThat(phrp.get_link("update"), is(notNullValue()));
		assertThat(phrp.get_links().size() + phrp.getLinks().size(), is(2));
	}

//	@Test
//	@DatabaseSetup(value = "DatabaseWithRejectedPHR.xml")
//	public void testUpdatePlantHireRequest() throws Exception {
//		ObjectMapper mapper = new ObjectMapper();
//		PlantResource plant = new PlantResource();
//		plant.add(new Link("http://localhost:3000/rest/plants/10002"));
//
//		Date startDate = new Date(1411344000000L);
//		Date endDate = new Date(1411516800000L);
//
//		PurchaseOrderResource porRequest = new PurchaseOrderResource();
//		porRequest.setIdRes(10001L);
//		porRequest.setStartDate(startDate);
//		porRequest.setEndDate(endDate);
//		porRequest.setPlant(plant);
//		porRequest.setCost(800f);
//
//		MvcResult result = mockMvc.perform(get("/rest/phrs/{id}", 10001L))
//				.andExpect(status().isOk()).andReturn();
//		PlantHireRequestResource phr = mapper.readValue(result.getResponse()
//				.getContentAsString(), PlantHireRequestResource.class);
//		assertThat(phr.get_link("update"), is(notNullValue()));
//
//		Link update = phr.get_link("update");
//		phr.setCost(800f);
//		result = mockMvc
//				.perform(
//						post(update.getHref()).contentType(
//								MediaType.APPLICATION_JSON).content(
//								mapper.writeValueAsString(phr)))
//				.andExpect(status().isOk()).andReturn();
//
//		phr = mapper.readValue(result.getResponse().getContentAsString(),
//				PlantHireRequestResource.class);
//		assertThat(phr.getCost(), is(equalTo(800f)));
//		assertThat(phr.getLink("self"), is(notNullValue()));
//		assertThat(phr.get_link("approve"), is(notNullValue()));
//		assertThat(phr.get_link("reject"), is(notNullValue()));
//		assertThat(phr.get_link("cancel"), is(notNullValue()));
//		assertThat(phr.get_links().size() + phr.getLinks().size(), is(4));
//	}

	// @Test
	// @DatabaseSetup(value="DatabaseWithRejectedPHR.xml")
	// public void testExtendPlantHireRequest() throws Exception {
	// ObjectMapper mapper = new ObjectMapper();
	//
	// UpdatePHRResource updatePHRR = new UpdatePHRResource();
	// updatePHRR.setStartDate(new LocalDate(2014, 9, 26).toDate());
	// updatePHRR.setEndDate(new LocalDate(2014, 9, 28).toDate());
	//
	// MvcResult result = mockMvc.perform(post("/rest/phrs/{id}/updates",10001L)
	// .contentType(MediaType.APPLICATION_JSON)
	// .content(mapper.writeValueAsString(updatePHRR))
	// )
	// .andExpect(status().isCreated())
	// .andReturn();
	//
	// updatePHRR = mapper.readValue(result.getResponse().getContentAsString(),
	// UpdatePHRResource.class);
	// assertThat(updatePHRR.getLink("self"), is(notNullValue()));
	// assertThat(updatePHRR.get_link("approve"), is(notNullValue()));
	// assertThat(updatePHRR.get_link("reject"), is(notNullValue()));
	// assertThat(updatePHRR.get_links().size() + updatePHRR.getLinks().size(),
	// is(3));
	// }

}
